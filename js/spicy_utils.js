/**
 Avtor: Igor Pesek
 Lastnik: Objem narave, d.o.o.
 Avtorske pravice zadržane, 2020
 Licence: Three.js
 Three.interaction.js
 Jquery.js
 **/

/**
 * Funkcija pripravi tabelo, ki služi kot tloris za objekt
 */
function pripraviTloris(){
   // pripravim tabelo za tloris
   var tloris = $("#spicy_table").find('tbody:last');
   for (i = 0; i < VRSTICE; i++){
       var tabelaVrstica = '<tr>';
       for (j = 0; j < STOLPCI; j++){
           tabelaVrstica += '<td id="' + (i-ZAMIKV) +'_' + (j-ZAMIKS) +'"></td>';
       }
       tabelaVrstica += '</tr>';
       tloris.append(tabelaVrstica);
   }

   $("#0_0").addClass("center_raster");

   // pomaknemo scroll bare na sredino
    var div = $('#spicy_table_wrapper');
    var tabela = $("#spicy_table");

    div.scrollLeft((tabela.width() - div.width()) / 2 + 30);
    div.scrollTop((tabela.height() - div.height()) / 2 + 10);
}

/**
 * Funkcija samo pobarva/odbarva celico v tlorisu
 *
 * @param elt  celica oz. td, ki je bil kliknjen
 */
function pobarvajCelicoTloris(x,y,barva){
    var elt = $("#" + x.toString() + "_" + y.toString());
    if ($(elt).hasClass("spicy_element")){
        $(elt).removeClass("spicy_element");
        $(elt).removeClass("raster");
        $(elt).removeClass("terasa");
        $(elt).removeClass("balkon");
    }
    else {
        $(elt).addClass("spicy_element");
        $(elt).addClass(barva);
    }
}

function manipuliranjeElement(x,y){
    if (OBJEKT.dobiElement(x,y) == null) {
        if ($(".imgFocus").length == 1){
            if ($(".imgFocus").first().hasClass("raster")){
                if (OBJEKT.smemDodati(x,y,1)) {
                    pobarvajCelicoTloris(x,y,"raster");
                    OBJEKT.dodajElement(x,y,1);
                    prikazDodajElement(x,y);
                    var koordinate = dolociPozicijoOsebe();
                    if (MODEL.oseba !== null && $("#prikaziOsebo").is(':checked')) {
                        MODEL.oseba.position.set(koordinate[0], -160, -koordinate[1]);
                    }
                }
                else {
                    console.log("Napaka pri dodajanju");
                    return false;
                }
            }
            else if ($(".imgFocus").first().hasClass("balkon")){
                if (OBJEKT.smemDodati(x,y,2)) {
                    pobarvajCelicoTloris(x,y,"balkon");
                    OBJEKT.dodajElement(x,y,2);
                    ustvariBalkon(x,y);
                    var koordinate = dolociPozicijoOsebe();
                    if (MODEL.oseba !== null && OSEBA_NALOZENA) {
                        MODEL.oseba.position.set(koordinate[0], -160, -koordinate[1]);
                    }
                }
                else {
                    console.log("Napaka pri dodajanju");
                    return false;
                }
            }
            else if ($(".imgFocus").first().hasClass("terasa")){
                if (OBJEKT.smemDodati(x,y,3)) {
                    pobarvajCelicoTloris(x,y,"terasa");
                    OBJEKT.dodajElement(x,y,3);
                    ustvariTerasa(x,y);
                    var koordinate = dolociPozicijoOsebe();
                    if (MODEL.oseba !== null && OSEBA_NALOZENA) {
                        MODEL.oseba.position.set(koordinate[0], -160, -koordinate[1]);
                    }
                }
                else {
                    console.log("Napaka pri dodajanju");
                    return false;
                }
            }
            else if ($(".imgFocus").first().hasClass("kos")){
                if (OBJEKT.smemIzbrisati(x,y)){
                    pobarvajCelicoTloris(x,y,null);
                    OBJEKT.odstraniElement(x,y);
                    prikazOdstraniElement(x,y);
                    var koordinate = dolociPozicijoOsebe();
                    if (MODEL.oseba !== null && OSEBA_NALOZENA ) {
                        MODEL.oseba.position.set(koordinate[0], -160, -koordinate[1]);
                    }
                    // mere prikazem samo v primeru, če je odkljukano
                    if ($("#prikaziMere").is(':checked')) {
                        pripraviMereObjekt();
                    }
                }
            }
            else {
                alert ("izberite raster ali teraso.");
                return true;
            }
        }
        else {
            alert ("izberite raster ali teraso.");
            return true;
        }
    }
    else { // element ze obstaja
        if ($(".imgFocus").first().hasClass("streha")){
            if (OBJEKT.dobiElement(x,y).dobiStreha() === 1 && $(".imgFocus").first().hasClass("ravna")){
                spremeniStreho (x,y, 0)
            }
            else if (OBJEKT.dobiElement(x,y).dobiStreha() === 0 && $(".imgFocus").first().hasClass("dvokapna")){
                spremeniStreho (x,y, 1)
            }
        }
        else if (OBJEKT.smemIzbrisati(x,y)){
            pobarvajCelicoTloris(x,y,null);
            OBJEKT.odstraniElement(x,y);
            prikazOdstraniElement(x,y);
            var koordinate = dolociPozicijoOsebe();
            if (MODEL.oseba !== null) {
                MODEL.oseba.position.set(koordinate[0], -160, -koordinate[1]);
            }

            // mere prikazem samo v primeru, če je odkljukano
            if ($("#prikaziMere").is(':checked')) {
                pripraviMereObjekt();
            }
        }
        else {
            console.log("Napaka pri brisanju");
            return false;
        }
    }
    posodobiOznakeRastrov(x, y);
    var cena = dolociCeno();
    $("#cena_objekta").html(cena + " €");
    var povrsina = (MODEL.elementi.length * 4.3).toFixed(2);
    $("#povrsina_objekta").html(povrsina.toString());
    $("#neto_povrsina_objekta").html((MODEL.elementi.length * 3.9).toFixed(2).toString());
    shraniTlorisKotSliko();
    shraniNarisKotSliko();

}



/**
 * sortiranje po x z bubble sort
 * @param seznam
 */
var sortiraj = function (seznam){
    var menjava = true;

    while (menjava) {
        menjava = false;
        for (var i = 0; i < seznam.length - 1; i++) {
            if (seznam[i].pozicijaX > seznam[i + 1].pozicijaX) {
                var x = seznam[i];
                seznam[i] = seznam[i + 1];
                seznam[i + 1] = x;
                menjava = true;
            }
        }
    }
    return seznam;
}


/**
 * Metoda spremeni streho na sklopu
 * @param x
 * @param y
 * @param tip
 */
function spremeniStreho (x,y, tip){
    var streha;
    var maxx, minx, pozX;
    var nov_seznam = [];

    //poiščem vse v tem stolpcu
    for (var i = 0; i < OBJEKT.elementi.length; i++) {
        if (OBJEKT.elementi[i].pozicijaY === y) {
                nov_seznam.push(OBJEKT.elementi[i]);
        }
    }

    //jih sortiram
    var seznam = sortiraj(nov_seznam);
    for (var j = 0; j < seznam.length; j++){
        if (seznam[j].pozicijaX === x){
            pozX = j;
            break;
        }
    }

    maxx = seznam[seznam.length - 1].pozicijaX;
    for (var j = pozX; j < seznam.length - 1; j++){
        if (seznam[j].pozicijaX + 1 !== seznam[j+1].pozicijaX){
            maxx = seznam[j].pozicijaX;
            break;
        }
    }

    minx = seznam[0].pozicijaX; //strazar
    for (var j = pozX; j >= 1; j--){
        if (seznam[j].pozicijaX - 1 !== seznam[j-1].pozicijaX){
            minx = seznam[j].pozicijaX;
            break;
        }
    }

    for (var i = 0; i < OBJEKT.elementi.length; i++){
        if (OBJEKT.elementi[i].pozicijaY === y){
            x = OBJEKT.elementi[i].pozicijaX;
            if (x >= minx && x <= maxx) {
                OBJEKT.elementi[i].nastaviStreha(tip);
                //unicim obstoječo
                SCENA.remove(MODEL.elementi[i].streha);
                SCENA.remove(MODEL.elementi[i].strehaCrteSpredaj);
                SCENA.remove(MODEL.elementi[i].strehaCrteZadaj);

                if (tip === 0) {  // ravna streha
                    streha = pripraviRavnoStreho(x, y);
                } else { //dvokapna
                    streha = pripraviDvokapnaStreha(x, y);
                    MODEL.elementi[i].strehaCrteSpredaj = narisiCrteStreha(x,y,1);
                    MODEL.elementi[i].strehaCrteZadaj = narisiCrteStreha(x,y,2);
                    if (!$("#prikaziCrte").is(':checked')){
                        mElement.strehaCrteSpredaj.visible = false;
                        mElement.strehaCrteZadaj.visible = false;
                    }
                }
                SCENA.add(streha);
                MODEL.elementi[i].streha = streha;
            }
        }
    }
    var cena = dolociCeno();
    $("#cena_objekta").html(cena + " €");
    shraniNarisKotSliko();

}



var dodatekClick = function (elt) {
    if ($(elt).hasClass("imgFocus")){
        $(elt).removeClass("imgFocus");
        return;
    }
    // vsem najprej izbrisemo ta razred.
    $(".imgFocus").each(function () {
        $(this).removeClass("imgFocus");
    });

    $(elt).addClass("imgFocus");
};

var izbrisiRazred = function (razred) {
    $("." + razred).each(function () {
        $(this).removeClass(razred);
    });
};

var posX = function (x,y,index){
    var list = [x * SIRINA, x * SIRINA, x * SIRINA + SIRINA/2 + 1, x * SIRINA - SIRINA/2 - 1];
    return list[index];
}
var posZ = function (x,y,index){
    var list = [-y * DOLZINA - DOLZINA/2 - 1, -y * DOLZINA + DOLZINA/2 + 1 ,-y * DOLZINA, -y * DOLZINA];
    return list[index];
}

/**
 * Metoda določi koordinate osebe glede na trenutni objekt.
 * @returns {*}
 */
var dolociPozicijoOsebe = function (){

    if (OBJEKT.elementi.length < 1)
        return [SIRINA,-DOLZINA/4];

    let okvir = dolociObjektOkvir();

    for (var y = okvir.minY ; y <= okvir.maxY; y++){
        for (var i = 0; i < OBJEKT.elementi.length; i++){
            if (OBJEKT.elementi[i].pozicijaY === y && OBJEKT.elementi[i].pozicijaX === okvir.maxX){
                return [(OBJEKT.elementi[i].pozicijaX + 1) * SIRINA, OBJEKT.elementi[i].pozicijaY * DOLZINA - DOLZINA/4]
            }
        }
    }

    let koordinate = [(okvir.maxX + 1) * SIRINA, okvir.maxY * DOLZINA + DOLZINA/4];
    return koordinate;
}

/**
 * metoda ob spremembi kljukice prikaze ali skrije mere.
 */
var onchPrikazMere = function (){
    // prikazemo mere
    if ($("#prikaziMere").is(':checked')) {
        pripraviMereObjekt();
    }
    else { // odstranimo mere
        odstraniMere();
    }
}

var osveziSkupneMere = function (){
    SCENA.remove(MODEL.meraD);
    MODEL.meraD = null;
    SCENA.remove(MODEL.meraS);
    MODEL.meraS = null;
    SCENA.remove(MODEL.puscicaD1);
    MODEL.puscicaD1 = null;
    SCENA.remove(MODEL.puscicaS1);
    MODEL.puscicaS1 = null;
    SCENA.remove(MODEL.puscicaD2);
    MODEL.puscicaD2 = null;
    SCENA.remove(MODEL.puscicaS2);
    MODEL.puscicaS2 = null;
    prikazSkupneMere();

}

var onchPrikazCrte = function (){
    var test = $("#prikaziCrte").is(':checked');
    for (var i = 0; i < MODEL.elementi.length; i++) {  //po rastrih
        //najprej manipuliram horizontalne črte
        if (test) {
            MODEL.elementi[i].hCrtaSpodaj.visible = true;
            MODEL.elementi[i].hCrtaZgoraj.visible = true;
            if (MODEL.elementi[i].strehaCrteSpredaj !== null) {
                MODEL.elementi[i].strehaCrteSpredaj.visible = true;
                MODEL.elementi[i].strehaCrteZadaj.visible = true;
            }
        } else {
            MODEL.elementi[i].hCrtaZgoraj.visible = false;
            MODEL.elementi[i].hCrtaSpodaj.visible = false;
            if (MODEL.elementi[i].strehaCrteSpredaj !== null) {
                MODEL.elementi[i].strehaCrteSpredaj.visible = false;
                MODEL.elementi[i].strehaCrteZadaj.visible = false;
            }
        }
        //terasa
        for (var m = 0; m < MODEL.elementi[i].terasa.length; m++) {
            if (MODEL.elementi[i].terasa[m].name === "crta"){
                if (test){
                    MODEL.elementi[i].terasa[m].visible = true;
                }
                else {
                    MODEL.elementi[i].terasa[m].visible = false;
                }
            }

        }

        for (var k = 2; k <= 3; k++) {  //po čelnih stenah
            for (var l = 0; l < 3; l++) {
                if (MODEL.elementi[i].stene[k].odprtina[l].crte !== null) {
                    if (test) {
                        MODEL.elementi[i].stene[k].odprtina[l].crte.visible = true;
                        MODEL.elementi[i].hCrtaSpodaj.visible = true;
                        MODEL.elementi[i].hCrtaZgoraj.visible = true;

                    } else {
                        MODEL.elementi[i].stene[k].odprtina[l].crte.visible = false;
                        MODEL.elementi[i].hCrtaZgoraj.visible = false;
                        MODEL.elementi[i].hCrtaSpodaj.visible = false;

                    }
                }

            }
        }
    }
}


var onchPrikazOseba = function (){
    if ($("#prikaziOsebo").is(':checked')) {
        var koordinate = dolociPozicijoOsebe();
        MODEL.oseba.position.set(koordinate[0],- 160,-koordinate[1]);
        MODEL.oseba.visible = true;
    }
    else {
        MODEL.oseba.visible = false;
    }
}

/**
 * Prilagojena metoda, ki doda končnico v ime slike
 * @param string
 * @returns {string}
 */
String.prototype.spicy_insert = function (string) {
    return this.slice(0, -4) + string + this.slice(-4);
};


var shraniSliko = function(){
    var link = document.createElement('a');
    link.download = "slikaNaris.png";
    link.href = shraniNarisKotSlikoJSON();
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}

var shraniSlikoTloris = function(){
    var link = document.createElement('a');
    link.download = "slikaTloris.png";
    link.href = shraniTlorisKotSlikoJSON();
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}


function shraniVPDF(){
    const { jsPDF } = window.jspdf;

    const doc = new jsPDF(
        {
            orientation: 'p',
            unit: 'mm',
            format: 'a4',
            putOnlyUsedFonts:true
        });
    doc.text("Pick&Build Objekt - Ponudba st. 2332/XX", 10, 10);

    doc.addImage(shraniPosnetekZaslonaKotSliko(), 'PNG',10,15,90,45);

    doc.text("Tloris objekta", 10, 100);
    doc.addImage(shraniTlorisKotSliko(), 'PNG', 10, 110,200, 200);

    doc.save("Pick&BuildObjekt.pdf");
}

var shraniPosnetekZaslonaKotSliko = function (){
    //posnetek zaslona
    camera.aspect = 200 / 100;
    camera.updateProjectionMatrix();
    renderer.setSize( 200, 100 );

    renderer.render( SCENA, camera, null, false );

    var dataUrl = renderer.domElement.toDataURL("image/png");

    camera.aspect = CONTAINER.offsetWidth/CONTAINER.offsetHeight ;
    camera.updateProjectionMatrix();
    renderer.setSize( CONTAINER.offsetWidth,CONTAINER.offsetHeight  );

    return dataUrl;
}


var pripraviSteber = function (x,y){
    var steber = new Konva.Rect({
        x: x,
        y: y,
        width: TLORIS_STEBER_S,
        height: TLORIS_STEBER_V-1,
        fill: '#ffffff',
        stroke: '#CF152D',
        strokeWidth: 2
    });

    return steber;
}

//pripravimo vertikalno steno
var pripraviHorSteno = function (x,y){
    var stena = new Konva.Rect({
        x: x,
        y: y,
        width: TLORIS_SIRINA,
        height: TLORIS_STEBER_V - 1,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });

    return stena;
}

//pripravimo Xframe
var pripraviXFrame = function (x,y, podlaga){
    var crta = new Konva.Line({
        points : [x+1,y + 1, x + TLORIS_SIRINA - 1, y + TLORIS_STEBER_V -2],
        stroke: '#ffffff',
        strokeWidth: 2,
        lineCap: 'square'
    });

    podlaga.add(crta);

    var crta = new Konva.Line({
        points : [x + 1, y + TLORIS_STEBER_V-2, x + TLORIS_SIRINA -1, y +1],
        stroke: '#ffffff',
        strokeWidth: 2,
        lineCap: 'square'
    });

    podlaga.add(crta);
}




//pripravimo vertikalno steno
var pripraviHorTankoSteno = function (x,y){
    var stena = new Konva.Line({
        points: [x+2,y,x+TLORIS_SIRINA-2, y],
        stroke: '#18355E',
        dash:[3,3],
        strokeWidth: 1,
        lineCap: 'square'
    });

    return stena;
}

//pripravimo horizontalno steno
var pripraviVerSteno = function (x,y){
    var stena = new Konva.Rect({
        x: x,
        y: y,
        width: TLORIS_STEBER_V,
        height: TLORIS_DOLZINA-1,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });

    return stena;
}

//pripravimo vertikalno tanko steno
var pripraviVerTankoSteno = function (x,y){
    var stena = new Konva.Line({
        points : [x+1,y + 2, x +1, y + TLORIS_DOLZINA - 2 ],
        stroke: '#18355E',
        dash:[3,3],
        strokeWidth: 1,
        lineCap: 'square'
    });

    return stena;
}

//pripravimo horizontalno odprtino
var pripraviHorOdprtino = function (x,y,stena,id, podlaga){
    var okno = new Konva.Rect({
        x: x + TLORIS_STEBER_S + TLORIS_ZAMIK,
        y: y + 1,
        width: TLORIS_ODPRTINA,
        height: TLORIS_STEBER_V - 2,
        fill: 'white',
        stroke: 'black',
        strokeWidth: 1
    });
    podlaga.add(okno);

    y = (stena === 0)? y + 1  - TLORIS_STEBER_V : y + 1  + TLORIS_STEBER_V + 15;

    var circle = new Konva.Circle({
        x: x + TLORIS_STEBER_S + TLORIS_ZAMIK + TLORIS_ODPRTINA/2 - 2,
        y: y,
        radius: 14,
        stroke: '#18355E',
        strokeWidth: 1,
    });
    podlaga.add(circle);

    var text = new Konva.Text({
        x : x + TLORIS_STEBER_S + TLORIS_ZAMIK + TLORIS_ODPRTINA/2 - 10,
        y : y - 6,
        text : "O" + id,
        fontSize: 15,
        fontStyle: 'bold',
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);
}

//pripravimo vertikalno odprtino
var pripraviVerOdprtinoSpredaj = function (x,y, start, dolzina, stena, id, podlaga){

    var okno = new Konva.Rect({
        x: x + 1,
        y: y + TLORIS_STEBER_V + 2* TLORIS_ZAMIK + start*(TLORIS_ODPRTINA + 2*TLORIS_ZAMIK),
        width: TLORIS_STEBER_V - 2,
        height:TLORIS_ODPRTINA + dolzina*(TLORIS_ODPRTINA + 2 * TLORIS_ZAMIK + 1),
        fill: 'white',
        stroke: 'black',
        strokeWidth: 1
    });

    podlaga.add(okno);

    var circle = new Konva.Circle({
        x: x + TLORIS_STEBER_V + 16,
        y: y + TLORIS_STEBER_V + 2* TLORIS_ZAMIK + start*(TLORIS_ODPRTINA + 2*TLORIS_ZAMIK) + (TLORIS_ODPRTINA + dolzina * (TLORIS_ODPRTINA + 2 * TLORIS_ZAMIK + 1))/2 - 1,
        radius: 14,
        stroke: '#18355E',
        strokeWidth: 1,
    });
    podlaga.add(circle);

    var text = new Konva.Text({
        x : x + TLORIS_STEBER_V + 7,
        y : y + TLORIS_STEBER_V + 2* TLORIS_ZAMIK + start*(TLORIS_ODPRTINA + 2*TLORIS_ZAMIK) + (TLORIS_ODPRTINA + dolzina * (TLORIS_ODPRTINA + 2 * TLORIS_ZAMIK + 1))/2 - 7,
        text : 'O' + id,
        fontSize: 15,
        fontStyle: 'bold',
        fontFamily: 'Roboto Mono',
        fill: '#18355E',

    });
    podlaga.add(text);

}

//pripravimo vertikalno odprtino
var pripraviVerOdprtinoZadaj = function (x,y, start, dolzina, stena, id, podlaga){

    if (dolzina === 2){
        start = 0;
    }
    else if (dolzina === 1 && start > 0){
        start = start - 1;
    }

    var okno = new Konva.Rect({
        x: x + 1,
        y: y + TLORIS_STEBER_V + 2* TLORIS_ZAMIK + start*(TLORIS_ODPRTINA + 2*TLORIS_ZAMIK),
        width: TLORIS_STEBER_V - 2,
        height:TLORIS_ODPRTINA + dolzina*(TLORIS_ODPRTINA + 2 * TLORIS_ZAMIK + 1),
        fill: 'white',
        fontStyle: 'bold',
        stroke: 'black',
        strokeWidth: 1
    });

    podlaga.add(okno);

    var circle = new Konva.Circle({
        x: x + TLORIS_STEBER_V - 36,
        y: y + TLORIS_STEBER_V + 2* TLORIS_ZAMIK + start*(TLORIS_ODPRTINA + 2*TLORIS_ZAMIK) + (TLORIS_ODPRTINA + dolzina * (TLORIS_ODPRTINA + 2 * TLORIS_ZAMIK + 1))/2 - 2,
        radius: 14,
        stroke: '#18355E',
        strokeWidth: 1,
    });
    podlaga.add(circle);

    var text = new Konva.Text({
        x : x - 25,
        y : y + TLORIS_STEBER_V + 2* TLORIS_ZAMIK + start*(TLORIS_ODPRTINA + 2*TLORIS_ZAMIK) + (TLORIS_ODPRTINA + dolzina * (TLORIS_ODPRTINA + 2 * TLORIS_ZAMIK + 1))/2 - 8,
        text : 'O' + id,
        fontSize: 15,
        fontFamily: 'Roboto Mono',
        fill: '#18355E',

    });
    podlaga.add(text);

}


var meraVerTlorisRaster = function(x, y, besedilo, podlaga){

    var crta = new Konva.Arrow({
            points: [x, y+4, x, y + TLORIS_DOLZINA-4],
            stroke: 1,
            strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x-TLORIS_STEBER_S - 10,
        y : y + TLORIS_DOLZINA/2 + 25,
        rotation : -90,
        text : besedilo,
        fontSize: 18,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}

var meraHorTlorisRaster = function(x, y, besedilo, podlaga){

    var crta = new Konva.Arrow({
        points: [x+4, y, x + TLORIS_SIRINA-4, y],
        stroke: 1,
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);


    //besedilo
    var text = new Konva.Text({
        x : x + TLORIS_SIRINA/2 - 25,
        y : y  + TLORIS_STEBER_S -1,
        text : besedilo,
        fontSize: 18,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}


/**
 * Metoda ustvari plosce z merami in jih prikaze
 */
var prikazTlorisSkupneMere = function (podlaga){
    var okvir = dolociObjektOkvir();

    let x = TLORIS_PADDING;
    let y = TLORIS_PADDING + (Math.abs(okvir.maxY - okvir.minY) + 1) * TLORIS_DOLZINA + 2.7*TLORIS_STEBER_V;

    var crta = new Konva.Arrow({
        points: [x +4 , y, x + (Math.abs(okvir.maxX - okvir.minX) + 1) * TLORIS_SIRINA-4, y],
        stroke: 2,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (Math.abs(okvir.maxX - okvir.minX) + 1) * TLORIS_SIRINA/2 - 25,
        y : y + 6,
        text : ((Math.abs(okvir.maxX - okvir.minX) + 1) * SIRINA).toString() + " cm",
        fontSize: 18,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

    // vertikalna skupna mera
    x = TLORIS_PADDING-2.2*TLORIS_STEBER_V;
    y = TLORIS_PADDING;

    crta = new Konva.Arrow({
        points: [x, y+4, x, y + (Math.abs(okvir.maxY - okvir.minY) + 1) * TLORIS_DOLZINA-4],
        stroke: 2,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    text = new Konva.Text({
        x : x - 20,
        y : y + Math.abs(okvir.maxY - okvir.minY + 1)* TLORIS_DOLZINA/2 + 25,
        rotation : -90,
        text : ((Math.abs(okvir.maxY - okvir.minY) + 1) * DOLZINA).toString() + " cm",
        fontSize: 18,
        fontFamily: 'Roboto Mono',
        fill: 'black',
    });
    podlaga.add(text);

}

var tlorisTipRastra = function (x,y, podlaga, besedilo){

    //besedilo
    var text = new Konva.Text({
        x : x + TLORIS_SIRINA/2 - 4,
        y : y + TLORIS_DOLZINA/2 - 10,
        text : besedilo,
        fontSize: 16,
        fontFamily: 'Roboto Mono',
        fill: 'black',
    });

    podlaga.add(text);
}


var balkonSafrura = function (x,y){
    podlaga = new Konva.Rect({
        x: x + 1,
        y: y + 1,
        width: TLORIS_SIRINA - 1,
        height: TLORIS_DOLZINA - 1,
        fillPatternImage: SRAFURA_SLIKA,
        //fillPatternRotation: 90,
        fillPatternScale: {x: 1, y: 1 },
        fillPatternRepeat : 'repeat',
        stroke: 'black',
        strokeWidth: 0
    });
    return podlaga;
}

var jeSkrajnoLevi = function (x,y){
    for (var i = 0; i < OBJEKT.elementi.length; i++){ //najprej poiscem vse rastre, ki so v y vrsti
        if (OBJEKT.elementi[i].pozicijaY === y && OBJEKT.elementi[i].pozicijaX > x){
            return false;
        }
    }
    return true;
}

var meraJeRobni = function (x,y){

    if (OBJEKT.dobiElement(x,y).dobiTipElementa() === "A"){ //če je B, je edini in če je stena vidna
        if (y + 1 + ZAMIKS < STOLPCI && OBJEKT.dobiElement(x,y + 1) == null){
            return true;
        }
    }
    else if (OBJEKT.dobiElement(x,y).dobiSteno(0).jeVidna()){
        return true;
    }
    else if (OBJEKT.dobiElement(x,y).dobiTipElementa() === "E" && y + 1 + ZAMIKS < STOLPCI && OBJEKT.dobiElement(x,y + 1) == null){
        return true;
    }
    return false;
}


var pripraviTlorisKotSliko = function(layer){
    var okvir = dolociObjektOkvir();

    let sirina = Math.abs(okvir.maxX - okvir.minX) + 1;
    let visina = Math.abs(okvir.maxY - okvir.minY) + 1;

    let objekt_sirina = sirina*(TLORIS_SIRINA + 16) + 40;
    let objekt_visina = visina*(TLORIS_DOLZINA + 10) + 40;

    // ta kvadrat je potreben, ker drugače skaliranje ne deluje
    var podlaga = new Konva.Rect({
        x: 0,
        y: 0,
        //fill: "yellow",
        width: objekt_sirina,
        height: objekt_visina,
    });
    layer.add(podlaga);

    for (var i = 0; i < OBJEKT.elementi.length; i++){
        let x = TLORIS_PADDING + Math.abs(OBJEKT.elementi[i].pozicijaX - okvir.maxX) * TLORIS_SIRINA;
        let y = TLORIS_PADDING + Math.abs(OBJEKT.elementi[i].pozicijaY - okvir.minY) * TLORIS_DOLZINA;

        let PX = OBJEKT.elementi[i].pozicijaX;
        let PY = OBJEKT.elementi[i].pozicijaY;

        var tip = OBJEKT.elementi[i].dobiTipElementa();
        var orientacija = OBJEKT.elementi[i].orientacija;

        if (tip === "D" || tip === "E"){
            if (orientacija === 1) {
                layer.add(balkonSafrura(x, y));
            }
            else {
                layer.add(balkonSafrura(x - 1, y));
            }
        }

        if (tip === "A" && OBJEKT.elementi.length> 1){
            tlorisTipRastra(x-10,y,layer,tip);
        }
        else if (tip === "B"){
            tlorisTipRastra(x+7,y,layer,tip);
        }
        else{
            tlorisTipRastra(x,y,layer,tip);
        }

        //stena levo
        if (OBJEKT.elementi[i].jeStenaVidna(1)) {
            layer.add(pripraviHorSteno(x,y));
            if (OBJEKT.elementi[i].dobiSteno(1).odprtina[0].obstaja){
                pripraviHorOdprtino(x,y,1,OBJEKT.elementi[i].dobiSteno(1).odprtina[0].id, layer);
            }
        }
        else { //narisem za balkon
            layer.add(pripraviHorTankoSteno(x,y));
        }

        //stena zadaj
        if (OBJEKT.elementi[i].jeStenaVidna(3)) {
            layer.add(pripraviVerSteno(x + TLORIS_SIRINA - TLORIS_STEBER_V,y));
            if (OBJEKT.elementi[i].dobiSteno(3).odprtina[0].obstaja && OBJEKT.elementi[i].dobiSteno(3).odprtina[0].zacetna){
                pripraviVerOdprtinoZadaj(x + TLORIS_SIRINA - TLORIS_STEBER_V,y, 2, OBJEKT.elementi[i].dobiSteno(3).odprtina[0].povezava.length, 3,OBJEKT.elementi[i].dobiSteno(3).odprtina[0].id,layer);
            }
            if (OBJEKT.elementi[i].dobiSteno(3).odprtina[1].obstaja && OBJEKT.elementi[i].dobiSteno(3).odprtina[1].zacetna){
                pripraviVerOdprtinoZadaj(x + TLORIS_SIRINA - TLORIS_STEBER_V,y, 1, OBJEKT.elementi[i].dobiSteno(3).odprtina[1].povezava.length, 3,OBJEKT.elementi[i].dobiSteno(3).odprtina[1].id,layer);
            }
            if (OBJEKT.elementi[i].dobiSteno(3).odprtina[2].obstaja && OBJEKT.elementi[i].dobiSteno(3).odprtina[2].zacetna){
                pripraviVerOdprtinoZadaj(x + TLORIS_SIRINA - TLORIS_STEBER_V,y, 0, OBJEKT.elementi[i].dobiSteno(3).odprtina[2].povezava.length, 3,OBJEKT.elementi[i].dobiSteno(3).odprtina[2].id,layer);
            }
        }

        // posebej izrisemo še sprednjo steno
       // if (tip === "D" || tip === "E"){
            if (orientacija === 1){
                layer.add(pripraviVerTankoSteno(x ,y));
            }
            else {
                layer.add(pripraviVerTankoSteno(x + TLORIS_SIRINA - TLORIS_STEBER_V/2,y));
            }
        //}

        //stena desno
        if (OBJEKT.elementi[i].jeStenaVidna(0)) {
            layer.add(pripraviHorSteno(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V));
            if (OBJEKT.elementi[i].dobiSteno(0).odprtina[0].obstaja){
                pripraviHorOdprtino(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V, 0, OBJEKT.elementi[i].dobiSteno(0).odprtina[0].id, layer);
            }
        }
        else { //narisem za balkon
            layer.add(pripraviHorTankoSteno(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V/2));
        }

        //stena spredaj
        if (OBJEKT.elementi[i].jeStenaVidna(2)) {
            layer.add(pripraviVerSteno(x ,y));
            if (OBJEKT.elementi[i].dobiSteno(2).odprtina[0].obstaja && OBJEKT.elementi[i].dobiSteno(2).odprtina[0].zacetna){
                pripraviVerOdprtinoSpredaj(x ,y, 0, OBJEKT.elementi[i].dobiSteno(2).odprtina[0].povezava.length, 2,OBJEKT.elementi[i].dobiSteno(2).odprtina[0].id,layer);
            }
            if (OBJEKT.elementi[i].dobiSteno(2).odprtina[1].obstaja && OBJEKT.elementi[i].dobiSteno(2).odprtina[1].zacetna){
                pripraviVerOdprtinoSpredaj(x ,y, 1, OBJEKT.elementi[i].dobiSteno(2).odprtina[1].povezava.length, 2,OBJEKT.elementi[i].dobiSteno(2).odprtina[1].id,layer);
            }
            if (OBJEKT.elementi[i].dobiSteno(2).odprtina[2].obstaja && OBJEKT.elementi[i].dobiSteno(2).odprtina[2].zacetna){
                pripraviVerOdprtinoSpredaj(x ,y, 2, OBJEKT.elementi[i].dobiSteno(2).odprtina[2].povezava.length, 2,OBJEKT.elementi[i].dobiSteno(2).odprtina[2].id,layer);
            }
        }

        if (tip === "A"){  // v A vedno narišemo X frame
            pripraviXFrame(x,y,layer);  //leva stena
            pripraviXFrame(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V,layer); //desna stena
        }
        else if (tip === "B" || tip === "D") { //če sta B ali E pa morata biti zadnja v vrsti, E (terasa) ne šteje
            if (PX + 1 + ZAMIKV < VRSTICE && (OBJEKT.dobiElement(PX + 1, PY) == null || OBJEKT.dobiElement(PX+ 1, PY).dobiTipElementa() === "E")) {
                layer.add(pripraviHorSteno(x,y));
                pripraviXFrame(x, y, layer);  //leva stena
                layer.add(pripraviHorSteno(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V));
                pripraviXFrame(x, y + TLORIS_DOLZINA - TLORIS_STEBER_V, layer); //desna stena

            }
        }


        //izris stebrov
        if (orientacija === 1){
            layer.add(pripraviSteber(x, y)); //steber levo zgoraj
            layer.add(pripraviSteber(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V)); //steber levo spodaj
            if (tip === "A") {
                layer.add(pripraviSteber(x + TLORIS_SIRINA - TLORIS_STEBER_S, y)); // steber desno zgoraj
                layer.add(pripraviSteber(x + TLORIS_SIRINA - TLORIS_STEBER_S, y + TLORIS_DOLZINA - TLORIS_STEBER_V));  // steber desno spodaj
            }
        }
        else {
            layer.add(pripraviSteber(x + TLORIS_SIRINA - TLORIS_STEBER_S, y)); // steber desno zgoraj
            layer.add(pripraviSteber(x + TLORIS_SIRINA - TLORIS_STEBER_S, y + TLORIS_DOLZINA - TLORIS_STEBER_V));  // steber desno spodaj
            if (tip === "A") {
                layer.add(pripraviSteber(x, y)); //steber levo zgoraj
                layer.add(pripraviSteber(x ,y + TLORIS_DOLZINA - TLORIS_STEBER_V)); //steber levo spodaj
            }
        }

        // izris mere za element
        if (jeSkrajnoLevi(OBJEKT.elementi[i].pozicijaX,OBJEKT.elementi[i].pozicijaY)) {
            meraVerTlorisRaster(x-TLORIS_STEBER_V,y,"406 cm", layer)
        }

        if (meraJeRobni(OBJEKT.elementi[i].pozicijaX,OBJEKT.elementi[i].pozicijaY)) {
            meraHorTlorisRaster(x, y + TLORIS_DOLZINA + TLORIS_STEBER_V , "122 cm", layer);
        }


    }

    // izris mer za celoten objekt
    if (OBJEKT.getSteviloElementov()>0) {
        prikazTlorisSkupneMere(layer);
    }

    layer.draw();

}

var shraniTlorisKotSliko = function(){
    var okvir = dolociObjektOkvir();
    // first we need to create a stage
    var stage = new Konva.Stage({
        container: 'tloris',   // id of container <div>
        width: 300,
        height: 300
    });

    var layer = new Konva.Layer();

    pripraviTlorisKotSliko( layer);

    let layerSize = layer.getClientRect();

    const stageSize = stage.getSize();
    const scaleX = stageSize.width  / layerSize.width ;
    const scaleY = stageSize.height / layerSize.height;
    const razmerje = Math.min(scaleX, scaleY);

    // zmanjšam podlago
    layer.scale({ x: razmerje, y: razmerje });
    layerSize = layer.getClientRect({
    })
    layer.position({
        x : 0,
        y : stageSize.height - layerSize.height - 5
    })

    layer.draw();

    stage.add(layer);
}

//pripravljeno za backend
var shraniTlorisKotSlikoJSON = function(){

    var layer = new Konva.Layer();

    pripraviTlorisKotSliko( layer);
    let layerSize = layer.getClientRect();

    // first we need to create a stage
    var stage = new Konva.Stage({
        container: 'slika_hidden',   // id of container <div>
        width: layerSize.width,
        height: layerSize.height
    });

    stage.add(layer);

    return stage.toDataURL();
}



var pripraviNarisTla = function (x,y,podlaga){

    var kvadrat = new Konva.Rect({
        x: x ,
        y: y + NARIS_STEBER_VISINA,
        width: NARIS_SIRINA,
        height:NARIS_STEBER_SIRINA,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });

    podlaga.add(kvadrat);
}

var pripraviNarisSteber = function (x,y,podlaga){

    var kvadrat = new Konva.Rect({
        x: x ,
        y: y,
        width: NARIS_STEBER_SIRINA,
        height:NARIS_STEBER_VISINA,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });
    podlaga.add(kvadrat);
}

var pripraviNarisVrata = function (x,y,podlaga){



    var kvadrat = new Konva.Rect({
        x: x ,
        y: y,
        width: NARIS_STEBER_SIRINA,
        height:NARIS_STEBER_VISINA - NARIS_VRATA,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0,

    });
    podlaga.add(kvadrat);


    var crta = new Konva.Line({
        points : [x +1 ,y + NARIS_STEBER_VISINA - NARIS_VRATA, x +1, y + NARIS_STEBER_VISINA ],
        stroke: '#18355E',
        strokeWidth: 1,
        dash : [5,5],
        lineCap: 'square'
    });
    podlaga.add(crta);


    var crta = new Konva.Line({
        points : [x - 1 + NARIS_STEBER_SIRINA - NARIS_ODPRTINA_SIRINA ,y + NARIS_STEBER_VISINA - NARIS_VRATA, x - 1 + NARIS_STEBER_SIRINA - NARIS_ODPRTINA_SIRINA, y + NARIS_STEBER_VISINA ],
        stroke: '#18355E',
        strokeWidth: 1,
        dash : [5,5],
        lineCap: 'square'
    });
    podlaga.add(crta);

}

var pripraviNarisStrop = function (x,y,podlaga){

    var kvadrat = new Konva.Rect({
        x: x + NARIS_STEBER_SIRINA,
        y: y,
        width: NARIS_SIRINA - NARIS_STEBER_SIRINA * 2,
        height:NARIS_STEBER_SIRINA,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });
    podlaga.add(kvadrat);
}

var pripraviNarisStropniki = function (x,y,podlaga){

    var kvadrat = new Konva.Rect({
        x: x + NARIS_STEBER_SIRINA,
        y: y,
        width: NARIS_SIRINA - NARIS_STEBER_SIRINA * 2,
        height:NARIS_ODPRTINA_SIRINA,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });
    podlaga.add(kvadrat);

    kvadrat = new Konva.Rect({
        x: x + NARIS_STEBER_SIRINA,
        y: y + NARIS_STEBER_SIRINA - NARIS_ODPRTINA_SIRINA,
        width: NARIS_SIRINA - NARIS_STEBER_SIRINA * 2,
        height:NARIS_ODPRTINA_SIRINA,
        fill: '#18355E',
        stroke: 'black',
        strokeWidth: 0
    });
    podlaga.add(kvadrat);
}

var pripraviNarisStreha = function (x,y,podlaga){

    var streha = new Konva.Line({
        points : [x + NARIS_STEBER_SIRINA/2,y, x + NARIS_SIRINA/2, y - NARIS_STREHA + NARIS_STEBER_SIRINA, x + NARIS_SIRINA - NARIS_STEBER_SIRINA/2,y ],
        stroke: '#18355E',
        strokeWidth: NARIS_STEBER_SIRINA,
        lineCap: 'round'
    });
    podlaga.add(streha);

    var wedge = new Konva.Wedge({
        x: x + NARIS_STEBER_SIRINA,
        y: y + 1,
        radius: 70,
        angle: 40,
        stroke: '#18355E',
        strokeWidth: 1,
        rotation: -40,
    });

    podlaga.add(wedge);
    //besedilo
    var text = new Konva.Text({
        x : x + 2.5*NARIS_STEBER_SIRINA ,
        y : y - 20,
        text : "40º",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: '#18355E',

    });
    podlaga.add(text);
}

var meraNarisNotranjiRaster = function(x, y, podlaga){

    var crta = new Konva.Arrow({
        points: [x + NARIS_STEBER_SIRINA + 4, y + NARIS_STEBER_VISINA + 2*NARIS_STEBER_SIRINA, x + NARIS_SIRINA - NARIS_STEBER_SIRINA - 4, y + NARIS_STEBER_VISINA + 2*NARIS_STEBER_SIRINA],
        stroke: 1,
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);


    //besedilo
    var text = new Konva.Text({
        x : x + NARIS_SIRINA/2 - 25,
        y : y  + NARIS_STEBER_VISINA + 2*NARIS_STEBER_SIRINA + 5,
        text : "366 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}

var meraNarisRaster = function(x, y, podlaga){

    var crta = new Konva.Arrow({
        points: [x + 4, y + NARIS_STEBER_VISINA + 3*NARIS_STEBER_SIRINA, x + NARIS_SIRINA - 4, y + NARIS_STEBER_VISINA + 3*NARIS_STEBER_SIRINA],
        stroke: 1,
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);


    //besedilo
    var text = new Konva.Text({
        x : x + NARIS_SIRINA/2 - 25,
        y : y  + NARIS_STEBER_VISINA + 3*NARIS_STEBER_SIRINA + 5,
        text : "416 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}

var meraNarisSkupna = function(x, y, sirina, podlaga){

    var crta = new Konva.Arrow({
        points: [x + 4, y + NARIS_STEBER_VISINA + 4*NARIS_STEBER_SIRINA, x + sirina* NARIS_SIRINA - 4, y + NARIS_STEBER_VISINA + 4*NARIS_STEBER_SIRINA],
        stroke: 1,
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);


    //besedilo
    var text = new Konva.Text({
        x : x + sirina * NARIS_SIRINA/2 - 30,
        y : y  + NARIS_STEBER_VISINA + 4*NARIS_STEBER_SIRINA + 5,
        text : 416*sirina + " mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}

var meraNarisVerRaster = function(x, y, sirina, podlaga){

    var crta = new Konva.Arrow({
        points: [x + (sirina)*(NARIS_SIRINA) + NARIS_STEBER_SIRINA  , y + NARIS_STEBER_SIRINA + 4, x + (sirina)*(NARIS_SIRINA) + NARIS_STEBER_SIRINA, y + NARIS_STEBER_VISINA - 4],
        stroke: 1,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (sirina)*(NARIS_SIRINA) + NARIS_STEBER_SIRINA + 8,
        y : y + (NARIS_STEBER_VISINA+NARIS_STEBER_SIRINA)/2 + 10,
        rotation : -90,
        text : "257 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

    var crta = new Konva.Arrow({
        points: [x + (sirina)*(NARIS_SIRINA) + 2*NARIS_STEBER_SIRINA  , y + 4, x + (sirina)*(NARIS_SIRINA) + 2*NARIS_STEBER_SIRINA, y + NARIS_STEBER_VISINA + NARIS_STEBER_SIRINA - 4],
        stroke: 1,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (sirina)*(NARIS_SIRINA) + 2*NARIS_STEBER_SIRINA + 8,
        y : y + (NARIS_STEBER_VISINA+NARIS_STEBER_SIRINA)/2 + 10,
        rotation : -90,
        text : "303 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}

var meraNarisVerRasterStreha = function(x, y, sirina, podlaga){

    var crta = new Konva.Arrow({
        points: [x + (sirina)*(NARIS_SIRINA) + NARIS_STEBER_SIRINA  , y + NARIS_STEBER_SIRINA + 4, x + (sirina)*(NARIS_SIRINA) + NARIS_STEBER_SIRINA, y + NARIS_STEBER_VISINA - 4],
        stroke: 1,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (sirina)*(NARIS_SIRINA) + NARIS_STEBER_SIRINA + 8,
        y : y + (NARIS_STEBER_VISINA+NARIS_STEBER_SIRINA)/2 + 10,
        rotation : -90,
        text : "257 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

    var crta = new Konva.Arrow({
        points: [x + (sirina)*(NARIS_SIRINA) + 2*NARIS_STEBER_SIRINA  , y + 4, x + (sirina)*(NARIS_SIRINA) + 2*NARIS_STEBER_SIRINA, y + NARIS_STEBER_VISINA + NARIS_STEBER_SIRINA - 4],
        stroke: 1,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (sirina)*(NARIS_SIRINA) + 2*NARIS_STEBER_SIRINA + 8,
        y : y + (NARIS_STEBER_VISINA+NARIS_STEBER_SIRINA)/2 + 10,
        rotation : -90,
        text : "303 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);


    var crta = new Konva.Arrow({
        points: [x + (sirina)*(NARIS_SIRINA) + 3*NARIS_STEBER_SIRINA  , y - NARIS_STREHA + 50 + 4, x + (sirina)*(NARIS_SIRINA) + 3*NARIS_STEBER_SIRINA, y + NARIS_STEBER_VISINA  - 4],
        stroke: 1,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (sirina)*(NARIS_SIRINA) + 3*NARIS_STEBER_SIRINA + 8,
        y : y - NARIS_STREHA +50 + (NARIS_STEBER_VISINA + NARIS_STREHA)/2 + 10,
        rotation : -90,
        text : "428 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

    var crta = new Konva.Arrow({
        points: [x + (sirina)*(NARIS_SIRINA) + 4*NARIS_STEBER_SIRINA  , y - NARIS_STREHA + 20 + 4, x + (sirina)*(NARIS_SIRINA) + 4*NARIS_STEBER_SIRINA, y + NARIS_STEBER_VISINA + NARIS_STEBER_SIRINA - 4],
        stroke: 1,
        strokeColor : "black",
        pointerAtBeginning: true,
        pointerLength: 2,
        pointerWidth: 1,
    });
    podlaga.add(crta);

    //besedilo
    var text = new Konva.Text({
        x : x + (sirina)*(NARIS_SIRINA) + 4*NARIS_STEBER_SIRINA + 8,
        y : y - NARIS_STREHA +50 + (NARIS_STEBER_VISINA + NARIS_STREHA)/2 + 10,
        rotation : -90,
        text : "481 mm",
        fontSize: 13,
        fontFamily: 'Roboto Mono',
        fill: 'black',

    });
    podlaga.add(text);

}

/*
Metoda preveri, ali ima kateri raster dvokapno streho
 */
var aliJeStreha = function(okvir){
        for (let j = 0; j < OBJEKT.elementi.length; j++){
            if (OBJEKT.elementi[j].dobiStreha() === 1){
                return true;
            }
        }
        return false;
}

var stolpecImaStreho = function(stolpecY){
    for (let j = 0; j < OBJEKT.elementi.length; j++){
        if (OBJEKT.elementi[j].pozicijaY === stolpecY && OBJEKT.elementi[j].dobiStreha() === 1){
            return true;
        }
    }
    return false;
}

var pripraviNarisKotSliko = function(stage, layer) {
    var okvir = dolociObjektOkvir();


    let sirina = Math.abs(okvir.maxY - okvir.minY) + 1;

    let imaStreho = aliJeStreha(okvir);

    let objekt_sirina = sirina*(NARIS_SIRINA + 16) + 5*NARIS_STEBER_SIRINA;
    let objekt_visina = NARIS_STEBER_VISINA + sirina*(NARIS_STEBER_SIRINA/2) + 15 + 100;
    let x = 0;
    let y = 0;
    if (imaStreho){
        objekt_visina = NARIS_STEBER_VISINA + sirina*(NARIS_STEBER_SIRINA/2) + NARIS_STREHA + 15 + 100;
        y = NARIS_STREHA;
    }

    // razširim podlago, da lahko dam vse gor
   // layer.width(objekt_sirina);
    //layer.height(objekt_visina);

    // ta kvadrat je potreben, ker drugače skaliranje ne deluje
    var podlaga = new Konva.Rect({
        x: 0,
        y: 0,
        //fill: "yellow",
        width: objekt_sirina,
        height: objekt_visina,
    });
    layer.add(podlaga);

    if (OBJEKT.elementi.length === 0) return;

    //narisemo kvadrat
    if (sirina === 1) {
        pripraviNarisTla(x, y, layer);
        pripraviNarisSteber(x, y, layer); //levi steber
        pripraviNarisSteber(x + NARIS_SIRINA - NARIS_STEBER_SIRINA, y, layer); //desni steber
        if (imaStreho) {
            pripraviNarisStropniki(x, y, layer);
            pripraviNarisStreha(x, y, layer);
        }
        else {
            pripraviNarisStrop(x,y,layer);
        }
        meraNarisNotranjiRaster(x,y,layer)
        meraNarisRaster(x,y,layer);

    }
    else { //imamo več vertikal
        //Najprej narišem levi raster
        pripraviNarisTla(x, y, layer);
        pripraviNarisSteber(x, y, layer); //levi steber
        pripraviNarisVrata(x + NARIS_SIRINA - NARIS_STEBER_SIRINA, y, layer); //desni steber
        if (stolpecImaStreho(okvir.minY)) {
            pripraviNarisStropniki(x, y, layer);
            pripraviNarisStreha(x, y, layer);
        }
        else {
            pripraviNarisStrop(x,y,layer);
        }
        meraNarisNotranjiRaster(x,y,layer)
        meraNarisRaster(x,y,layer);

        //vmesni rastri
        for (let i = 1; i < sirina - 1; i++){
            x = i*(NARIS_SIRINA);
            pripraviNarisTla(x, y, layer);
            pripraviNarisVrata(x, y, layer); //levi steber
            pripraviNarisVrata(x + NARIS_SIRINA - NARIS_STEBER_SIRINA, y, layer); //desni steber
            if (stolpecImaStreho(okvir.minY + i)) {
                pripraviNarisStropniki(x, y, layer);
                pripraviNarisStreha(x, y, layer);
            }
            else {
                pripraviNarisStrop(x,y,layer);
            }
            meraNarisNotranjiRaster(x,y,layer)
            meraNarisRaster(x,y,layer);
        }

        // desni raster
        x = (sirina-1)*(NARIS_SIRINA);
        pripraviNarisTla(x, y, layer);
        pripraviNarisVrata(x, y, layer); //levi steber
        pripraviNarisSteber(x + NARIS_SIRINA - NARIS_STEBER_SIRINA, y, layer); //desni steber
        if (stolpecImaStreho(okvir.maxY)) {
            pripraviNarisStropniki(x, y, layer);
            pripraviNarisStreha(x, y, layer);
        }
        else {
            pripraviNarisStrop(x,y,layer);
        }
        meraNarisNotranjiRaster(x,y,layer)
        meraNarisRaster(x,y,layer);
    }
    meraNarisSkupna(0,y, sirina,layer);
    if (imaStreho) {
        meraNarisVerRasterStreha(0, y, sirina, layer);
    }
    else {
        meraNarisVerRaster(0, y, sirina, layer);
    }

}

var shraniNarisKotSliko = function() {

    let stageWidth = 300;
    let stageHeight = 300;

    // ustvarim oder, velikosti kot div
    var stage = new Konva.Stage({
        container: 'naris',   // id of container <div>
        x: 10,
        y: 10,
        width: stageWidth,
        height: stageHeight,
    });

    // podlaga
    var layer = new Konva.Layer({});

    pripraviNarisKotSliko(stage, layer);


    //zmanjšam, da ustreza kvadratu
    let layerSize = layer.getClientRect({
    })

    const stageSize = stage.getSize();
    const scaleX = stageSize.width  / layerSize.width ;
    const scaleY = stageSize.height / layerSize.height;
    const razmerje = Math.min(scaleX, scaleY);

    // zmanjšam podlago
    layer.scale({ x: razmerje, y: razmerje });
    layerSize = layer.getClientRect({
    })
    layer.position({
        x : 0,
        y : stageSize.height - layerSize.height - 5
    })

    layer.draw();
    stage.add(layer);
}

// pripravljeno za klice v backend
var shraniNarisKotSlikoJSON = function() {

    var okvir = dolociObjektOkvir();
    let sirina = Math.abs(okvir.maxY - okvir.minY) + 1;

    let imaStreho = aliJeStreha(okvir);

    let objekt_sirina = sirina*(NARIS_SIRINA + 16) + 100;
    let objekt_visina = NARIS_STEBER_VISINA + sirina*(NARIS_STEBER_SIRINA/2) + 15 + 120;
    if (imaStreho){
        objekt_visina = NARIS_STEBER_VISINA + sirina*(NARIS_STEBER_SIRINA/2) + NARIS_STREHA + 15 + 120;
    }


    // ustvarim oder, velikosti kot div
    var stage = new Konva.Stage({
        container: 'slika_hidden',   // id of container <div>
        x: 10,
        y: 10,
        width: objekt_sirina,
        height: objekt_visina,
    });

    // podlaga
    var layer = new Konva.Layer({});

    pripraviNarisKotSliko(stage,layer);
    layer.draw();
    stage.add(layer);

    return stage.toDataURL();
}

var preberiVNovObjekt = function (koda){
    var obj;
    try {
        obj = JSON.parse(koda);
    }
    catch (e) {
        obj = zipson.parse(koda);
    }
    let verzija = obj.version;

    OBJEKT = Object.assign(OBJEKT, obj);
    OBJEKT.elementi = [];
    for (var i = 0; i < obj.elementi.length; i++){
        if (obj.elementi[i].pozicijaX === undefined)  // ce je slucajno kakšen ostanek elementa
            continue;

        var element;
        if (verzija === 1){
            if (obj.elementi[i].terasa === false){
                element = OBJEKT.dodajElement(obj.elementi[i].pozicijaX,obj.elementi[i].pozicijaY, 1); //dodam kot raster
            }
            else {
                element = OBJEKT.dodajElement(obj.elementi[i].pozicijaX, obj.elementi[i].pozicijaY, 2); // dodam kot balkon //sprememba
            }
        }
        else if (verzija === 2){
            element = OBJEKT.dodajElement(obj.elementi[i].pozicijaX,obj.elementi[i].pozicijaY, obj.elementi[i].oblika);
        }

        element.streha = obj.elementi[i].streha;

        if (element.oblika === 2){   //pobarvam tloris
            pobarvajCelicoTloris(obj.elementi[i].pozicijaX, obj.elementi[i].pozicijaY, "balkon");
        }
        else if (element.oblika === 1) {
            pobarvajCelicoTloris(obj.elementi[i].pozicijaX,obj.elementi[i].pozicijaY,"raster");
        }
        else {
            pobarvajCelicoTloris(obj.elementi[i].pozicijaX,obj.elementi[i].pozicijaY,"terasa");
        }

        element.stene = [new Stena(0),new Stena(1),new Stena(2),new Stena(3),new Stena(4),new Stena(5)];
        for (var k = 0; k < 6; k++){ // po vseh stenah
            var stena = OBJEKT.elementi[i].stene[k];
            stena.vidna = obj.elementi[i].stene[k].vidna;
            stena.odprtina = [new Odprtina(0),new Odprtina(1), new Odprtina(2)];
            for (var l = 0; l < 3; l++){
                var odprtina = OBJEKT.elementi[i].stene[k].odprtina[l];
                odprtina.obstaja = obj.elementi[i].stene[k].odprtina[l].obstaja;
                odprtina.id = obj.elementi[i].stene[k].odprtina[l].id ;
                odprtina.pozicija = obj.elementi[i].stene[k].odprtina[l].pozicija;
                odprtina.povezava = obj.elementi[i].stene[k].odprtina[l].povezava;
                odprtina.zacetna = obj.elementi[i].stene[k].odprtina[l].zacetna;
            }
        }
    }
}


/**
 * metoda izbriše iz scene 3D elemente prejšnjega modela.
 */
var novObjekt = function (){
    odstraniMere();
    for (var i = 0; i < MODEL.elementi.length; i++) {  //po rastrih
        for (var k = 0; k <= 3; k++) {  //po  stenah
            for (var l = 0; l < 3; l++) {  //odprtine
                SCENA.remove(MODEL.elementi[i].stene[k].odprtina[l].crte);
                SCENA.remove(MODEL.elementi[i].stene[k].odprtina[l].ravnina);
            }
        }
        SCENA.remove(MODEL.elementi[i].streha);
        SCENA.remove(MODEL.elementi[i].kocka);
        SCENA.remove(MODEL.elementi[i].hCrtaSpodaj);
        SCENA.remove(MODEL.elementi[i].hCrtaZgoraj);
        SCENA.remove(MODEL.elementi[i].strehaCrteSpredaj);
        SCENA.remove(MODEL.elementi[i].strehaCrteZadaj);
        SCENA.remove(MODEL.elementi[i].tipDesno);
        SCENA.remove(MODEL.elementi[i].tipLevo);

        for (var k = 0; k <= MODEL.elementi[i].terasa.length; k++) {  //po  stenah
            SCENA.remove(MODEL.elementi[i].terasa[k]);
        }
        pobarvajCelicoTloris(OBJEKT.elementi[i].pozicijaX,OBJEKT.elementi[i].pozicijaY);
    }


    MODEL = new ModelObjekt();
    OBJEKT = new Objekt();
    shraniTlorisKotSliko();
    shraniNarisKotSliko();
    MODEL.oseba = OSEBA;
    onchPrikazOseba();

}

const preberiDatoteko = (file) => {

    const reader = new FileReader();
    reader.addEventListener('load', (event) => {
        const koda = event.target.result;
        novObjekt();
        preberiVNovObjekt(koda);
        OBJEKT.posodobiElemente();
        ustvariModelObjekta();
        premikKameraSpredaj();
        var cena = dolociCeno();
        $("#cena_objekta").html(cena + " €");
        var povrsina = (MODEL.elementi.length * 4.3).toFixed(2);
        $("#povrsina_objekta").html(povrsina.toString());
        $("#neto_povrsina_objekta").html((MODEL.elementi.length * 3.9).toFixed(2).toString());
        shraniTlorisKotSliko();
        shraniNarisKotSliko();
        MODEL.oseba = OSEBA;
        onchPrikazOseba();
    });

    reader.readAsText(file, "text/plain;charset=utf-8");

}

var naloziDatoteko = function (){
    const file = document.getElementById('input').files[0];
    if (file) {
        preberiDatoteko(file);
        $.modal.close();
    }
}

// https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript/901144#901144
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * Izračun okvirja objekta. Vrnemo skrajne leve, desne, zgornje in spodnje koordinate objekta
 */

var dolociObjektOkvir = function() {
    if (OBJEKT.elementi.length < 1)
        return {maxX: 0,
            minX : 0,
            maxY : 0,
            minY : 0};

    var maxX = -10, maxY = -10, minX = 10, minY = 10;
    for (var i = 0; i < OBJEKT.elementi.length; i++){
        if (OBJEKT.elementi[i].pozicijaY >= maxY){
            maxY = OBJEKT.elementi[i].pozicijaY;
        }
        if (OBJEKT.elementi[i].pozicijaY < minY){
            minY = OBJEKT.elementi[i].pozicijaY;
        }
        if (OBJEKT.elementi[i].pozicijaX >= maxX){
            maxX = OBJEKT.elementi[i].pozicijaX;
        }
        if (OBJEKT.elementi[i].pozicijaX < minX){
            minX = OBJEKT.elementi[i].pozicijaX;
        }
    }



    return {maxX: maxX,
        minX : minX,
        maxY : maxY,
        minY : minY};
}


var odstraniMere = function(){
    for (var i = 0; i < MODEL.elementi.length; i++){
        SCENA.remove(MODEL.elementi[i].meraD);
        MODEL.elementi[i].meraD = null;
        SCENA.remove(MODEL.elementi[i].meraS);
        MODEL.elementi[i].meraS = null;
        SCENA.remove(MODEL.elementi[i].puscicaD1);
        MODEL.elementi[i].puscicaD1 = null;
        SCENA.remove(MODEL.elementi[i].puscicaS1);
        MODEL.elementi[i].puscicaS1 = null;
        SCENA.remove(MODEL.elementi[i].puscicaD2);
        MODEL.elementi[i].puscicaD2 = null;
        SCENA.remove(MODEL.elementi[i].puscicaS2);
        MODEL.elementi[i].puscicaS2 = null;
    }
    SCENA.remove(MODEL.meraD);
    MODEL.meraD = null;
    SCENA.remove(MODEL.meraS);
    MODEL.meraS = null;
    SCENA.remove(MODEL.puscicaD1);
    MODEL.puscicaD1 = null;
    SCENA.remove(MODEL.puscicaS1);
    MODEL.puscicaS1 = null;
    SCENA.remove(MODEL.puscicaD2);
    MODEL.puscicaD2 = null;
    SCENA.remove(MODEL.puscicaS2);
    MODEL.puscicaS2 = null;
}



/**
 *  Premikanje kamere - različni pogledi
 */
//TODO čisto na robu ne deluje pravilno.
var premikKameraDesno = function (){

    let okvir = dolociObjektOkvir();
    var koordX = okvir.minX;
    let razlika = Math.abs(okvir.minX - okvir.maxX);
    if (razlika >= 1){
        koordX = okvir.maxX - razlika / 2.0;
    }
    controls.setLookAt(koordX * SIRINA , OSEBA_VISINA,  - (okvir.maxY + 4) * DOLZINA ,
        koordX * SIRINA,VISINA / 2, - (okvir.maxY) * DOLZINA,false);
    controls.update();

}

var premikKameraSpredaj = function (){

    let okvir = dolociObjektOkvir();

    var koordY = okvir.minY;
    let razlika = Math.abs(okvir.minY - okvir.maxY);
    if (razlika >= 1){
        koordY = okvir.maxY - razlika / 2.0;
    }

    controls.setLookAt(okvir.maxX * SIRINA + 6 * DOLZINA , OSEBA_VISINA,  - koordY * DOLZINA , okvir.maxX * SIRINA,OSEBA_VISINA, - koordY * DOLZINA,false);
    controls.update();
}

/**
 *  Premikanje kamere
 */

var premikKameraLevo = function (){

    let okvir = dolociObjektOkvir();
    var koordX = okvir.minX;
    let razlika = Math.abs(okvir.minX - okvir.maxX);
    if (razlika >= 1){
        koordX = okvir.maxX - razlika / 2.0;
    }
    controls.setLookAt(koordX * SIRINA , OSEBA_VISINA,  (okvir.maxY + 4) * DOLZINA ,
        koordX * SIRINA,VISINA / 2, - (okvir.maxY) * DOLZINA,false);
    controls.update();
}

var premikKameraZadaj = function (){

    let okvir = dolociObjektOkvir();

    var koordY = okvir.minY;
    let razlika = Math.abs(okvir.minY - okvir.maxY);
    if (razlika >= 1){
        koordY = okvir.maxY - razlika / 2.0;
    }

    controls.setLookAt(- okvir.minX * SIRINA - 6 * DOLZINA , OSEBA_VISINA,  - koordY *  DOLZINA , okvir.minX * SIRINA,VISINA / 2, - koordY * DOLZINA,false);
    controls.update();
}


var premikKameraTloris = function (){

    let okvir = dolociObjektOkvir();

    var koordY = okvir.minY;
    let razlika = Math.abs(okvir.minY - okvir.maxY);
    if (razlika >= 1){
        koordY = okvir.maxY - razlika / 2.0;
    }

    var koordX = okvir.minX;
    razlika = Math.abs(okvir.minX - okvir.maxX);
    if (razlika >= 1){
        koordX = okvir.maxX - razlika / 2.0;
    }


    controls.setLookAt(koordX * SIRINA  , 10 * OSEBA_VISINA,  - koordY *  DOLZINA , koordX * SIRINA ,VISINA / 2, - koordY * DOLZINA,false);
    controls.rotate(  90 * THREE.Math.DEG2RAD, 0, false );
    controls.update();
}

var premikKameraTlorisMere = function (){

    let okvir = dolociObjektOkvir();

    controls.setLookAt(okvir.maxX * SIRINA  , 10 * OSEBA_VISINA,  - okvir.maxY *  DOLZINA - DOLZINA/2 , okvir.maxX  * SIRINA ,VISINA / 2, - okvir.maxY * DOLZINA - DOLZINA/2,false);
    controls.rotate(  90 * THREE.Math.DEG2RAD, 0, false );
    controls.update()

    if (!$("#prikaziMere").is(':checked')) {
        $("#prikaziMere").prop('checked', true);
        //posamezne mere
        pripraviMereObjekt();

    }
}


/**
 * spredaj desno
 */
var premikKameraAksoSD = function (){

    let okvir = dolociObjektOkvir();
    controls.setLookAt( okvir.maxX * SIRINA + 3 * DOLZINA , 3 * VISINA,  - okvir.maxY *  DOLZINA - 3 * DOLZINA, okvir.maxX * SIRINA, OSEBA_VISINA , - okvir.maxY * DOLZINA,false);
    controls.update();
}

/**
 * Zadaj desno
 */
var premikKameraAksoZD = function (){

    let okvir = dolociObjektOkvir();
    controls.setLookAt( okvir.minX * SIRINA - 3 * DOLZINA , 3 * VISINA,  - okvir.maxY *  DOLZINA - 3 * DOLZINA, okvir.minX * SIRINA, OSEBA_VISINA , - okvir.maxY * DOLZINA,false);
    controls.update();
}

/**
 * Spredaj levo
 */
var premikKameraAksoSL = function (){

    let okvir = dolociObjektOkvir();
    controls.setLookAt( okvir.maxX * SIRINA + 3 * DOLZINA , 3 * VISINA,  - okvir.minY *  DOLZINA + 3 * DOLZINA, okvir.maxX * SIRINA, OSEBA_VISINA , - okvir.minY * DOLZINA,false);
    controls.update();
}

/**
 * Zadaj levo
 */
var premikKameraAksoZL = function (){

    let okvir = dolociObjektOkvir();
    controls.setLookAt( okvir.minX * SIRINA - 3 * DOLZINA , 3 * VISINA,  - okvir.minY *  DOLZINA + 3 * DOLZINA, okvir.minX * SIRINA, OSEBA_VISINA , - okvir.minY * DOLZINA,false);
    controls.update();
}

/**********************************************************************
 *  Preverjanje ali smemo izbrisati nek raster
 */

var element2String = function (element){
    return element.pozicijaX + "_" + element.pozicijaY;
}

var DFS = function(vertex, obiskani, sosedi){
    obiskani[vertex] = 1;

    for (var i = 0; i < sosedi[vertex].length; i++){
        if (obiskani[sosedi[vertex][i]] === 0) {
            DFS(sosedi[vertex][i], obiskani, sosedi);
        }
    }

}

var preveriObjektZaIzbris = function (izbrisX,izbrisY){
    var sosedi = []; // matrika sosedi sosedov
    var obiskani = [];
    for (var index = 0; index < OBJEKT.elementi.length; index++){  // pregledamo celoten obkjekt in shranimo
        var x = OBJEKT.elementi[index].pozicijaX;
        var y = OBJEKT.elementi[index].pozicijaY;
        if (!(x === izbrisX && y === izbrisY)) { // trenutnega rastra ne vključim zraven
            sosedi[element2String(OBJEKT.elementi[index])] = [];
            obiskani[element2String(OBJEKT.elementi[index])] = 0;
            // dodamo jih kot sosede
            if (x + 1 + ZAMIKV < VRSTICE && OBJEKT.dobiElement(x + 1, y) != null && (!((x + 1) == izbrisX && y == izbrisY))) {
                sosedi[element2String(OBJEKT.elementi[index])].push(element2String(OBJEKT.dobiElement(x + 1, y)));
            }
            if (x - 1 + ZAMIKV >= 0 && OBJEKT.dobiElement(x - 1, y) != null && (!((x - 1) == izbrisX && y == izbrisY))) {
                sosedi[element2String(OBJEKT.elementi[index])].push(element2String(OBJEKT.dobiElement(x - 1, y)));
            }
            if (y + 1 + ZAMIKS < STOLPCI && OBJEKT.dobiElement(x, y + 1) != null && (!(x == izbrisX && (y+1) == izbrisY))) {
                sosedi[element2String(OBJEKT.elementi[index])].push(element2String(OBJEKT.dobiElement(x, y + 1)));
            }
            if (y - 1 + ZAMIKS >= 0 && OBJEKT.dobiElement(x, y - 1) != null && (!(x == izbrisX && (y - 1) == izbrisY))) {
                sosedi[element2String(OBJEKT.elementi[index])].push(element2String(OBJEKT.dobiElement(x, y - 1)));
            }
        }
    }

    DFS(Object.keys(sosedi)[0],obiskani,sosedi);

    for (var i = 0; i < Object.keys(obiskani).length; i++){
        if (obiskani[Object.keys(obiskani)[i]] === 0){
            return false;
        }
    }
    return true;
}

var izpisiPozicijo = function (){
    console.log("PA= " + controls.polarAngle);
    console.log("AA= " + controls.azimuthAngle);
}


/**** AJAX klici * */

/**
 * metoda doloci ceno objekta Uporabim AJAX klic v backend z axios knjižnico
 * @returns {number}
 */
var dolociCeno = function (){
    var cena = 100;

    // axios.post('/api/calculate', {
    //     objekt : JSON.stringify(OBJEKT)
    // })
    //     .then(function (response) {
    //         if (response.status === 200){
    //             cena = response.data.cena.toString();
    //         }
    //         else {
    //             cena = "napaka";
    //             console.log(response);
    //         }
    //     })
    //     .catch(function (error) {
    //         //console.log(error);
    //     });

    return cena;
}

function shraniObjekt(){
    // Ko pride v produkcijo
    var blob = new Blob([JSON.stringify(OBJEKT)], {type: "text/plain;charset=utf-8"})
    saveAs(blob, "Pick&BuildObjekt.pbo");

    // axios.post('/configuration/project/save', {
    //     objekt : JSON.stringify(OBJEKT),
    //     // tloris?
    // })
    //     .then(function (response) {
    //         if (response.status === 200){
    //              console.log("objekt shranjen");  // TODO dolociti kaj se zgodi, če shranjevanje uspe.
    //         }
    //         else {
    //             console.log("napaka");
    //         }
    //     })
    //     .catch(function (error) {
    //         //console.log(error);
    //     });
}

// pridobimo objekt iz backenda glede na hashid
var ustvariNoVObjektIzKode = function (oid){

    axios.get('/configuration/model', {
        params: {
            id: oid
        }
    })
        .then(function (response) {
            if (response.status === 200){
                var koda = response.data.objekt;
                if (koda.length < 1){
                    return;
                }
                novObjekt();
                preberiVNovObjekt(koda);
                OBJEKT.posodobiElemente();
                ustvariModelObjekta();
                premikKameraSpredaj();
                var cena = response.data.cena; //
                $("#cena_objekta").html(cena + " €");
                var povrsina = (MODEL.elementi.length * 4.3).toFixed(2);
                $("#povrsina_objekta").html(povrsina.toString());
                $("#neto_povrsina_objekta").html((MODEL.elementi.length * 3.9).toFixed(2).toString());
                shraniTlorisKotSliko();
                shraniNarisKotSliko();
                MODEL.oseba = OSEBA;
                onchPrikazOseba();
            }

        })
        .catch(function (error) {
            console.log(error);
        });
}

var dodajVVozicek = function(){

    axios.post('/cart/addtocart', {
        objekt : JSON.stringify(OBJEKT),
        tloris : shraniTlorisKotSlikoJSON(),
        naris : shraniNarisKotSlikoJSON(),
        posnetekS : shraniPosnetekZaslonaKotSliko(),
        posnetekZ : shraniPosnetekZaslonaKotSliko()
    })
        .then(function (response) {
            if (response.status === 200){
                 console.log("objekt dodan v voziček");  // TODO dolociti kaj se zgodi, če shranjevanje uspe.
            }
            else {
                console.log("napaka");
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}


