/**
 Avtor: Igor Pesek
 Lastnik: Objem narave, d.o.o.
 Avtorske pravice zadržane, 2020
 Licence: Three.js
           Three.interaction.js
           Jquery.js
 **/

/**
 *  Razred za objekt, vsebuje elemente.
 */
class Objekt{
    constructor(){
        this.version = 2;
        this.barvaFasada = '#080808';
        this.barvaStreha = '#080808';
        this.elementi = [];
    };


    /**
     * funkcija poisce v seznamu elementov tistega s enakimi koordinatami
     * @param x x-koordinata v tabeli
     * @param y y-koordinata v tabeli
     * @returns {null|*}
     */
    dobiElement(x,y){
        for (var index = 0; index < this.elementi.length; index++){
            if (this.elementi[index].pozicijaX == x && this.elementi[index].pozicijaY == y)
                return this.elementi[index];
        }

        return null;
    }

    /**
     * metoda odstrani element iz objekta
     * @param i
     * @param j
     * @returns {boolean}
     */
    odstraniElement(i,j){
        for (var index = 0; index < this.elementi.length; index++){
            if (this.elementi[index].pozicijaX === i && this.elementi[index].pozicijaY === j){
                let oblika = this.elementi[index].oblika;
                this.elementi.splice(index,1);
                this.dolociTipRastrov(i,j);
                if (oblika === 1)
                    this.posodobiStene(i,j);
                this.posodobiElemente();
                return true;
            }
        }
        return false;
    }

    /**
     * metoda doda nov element v objekt
     * @param i
     * @param j
     */
    dodajElement(i,j,tip) {
        var element = new Element( i, j,tip);
        this.elementi.push(element);
        this.dolociTipRastrov(i,j);
        if (tip === 1) {
            this.posodobiStene(i,j,true);
        }
        this.posodobiStreho(i,j);
        this.posodobiElemente();
        //doloci orientacijo
        if (element.dobiTipElementa() === "D" || element.dobiTipElementa() === "E"){
            if (this.dobiElement(i+1,j) !== null) {
                if (this.dobiElement(i+1,j).dobiTipElementa() === "A" || this.dobiElement(i+1,j).dobiTipElementa() === "E"  ||  this.dobiElement(i+1,j).dobiTipElementa() === "D" ) {
                    element.orientacija = -1;
                }
            }
            //za balkon in teraso popravim še stene.
            element.dobiSteno(2).setVidna(false);  //spredaj
            element.dobiSteno(3).setVidna(false);  //zadaj

            if (element.dobiTipElementa() === "E"){  //zbrisem se stranski steni
                element.dobiSteno(1).setVidna(false);
                element.dobiSteno(0).setVidna(false);
            }
        }
        return element;
    }

    /**
     * Metoda preveri, če sploh lahko dodamo trenutno celico v tabeli k objektu.
     * @param id seznam, id[0].. x, id[1]..y koordinata
     * @returns {boolean}
     */
    smemDodati(x,y, tip){
        if (this.getSteviloElementov() === 0 && tip === 1) {
            return true;
        }
        else {
            // imeti moramo vsaj enega soseda
            if (x + 1 + ZAMIKV < VRSTICE && this.dobiElement(x + 1,y) !== null){
                if (this.dobiElement(x + 1,y).dobiOblikoElementa() === 2 && tip === 1) {
                    return false;
                }
                return true;
            }
            if (x - 1 + ZAMIKV >= 0 && this.dobiElement(x - 1,y) != null) {
                if (this.dobiElement(x - 1,y).dobiOblikoElementa() === 2 && tip === 1) {
                    return false;
                }
                return true;
            }
            if (y + 1 + ZAMIKS < STOLPCI && this.dobiElement(x,y + 1) != null) {
                if (tip === 2)
                    return false;
                return true;
            }
            if (y - 1 + ZAMIKS >= 0 && this.dobiElement(x ,y - 1) != null) {
                if (tip === 2)
                    return false;
                return true;
            }
            return false;
        }
    }

    /**
     * Metoda preveri, če smemo izbrisati trenutno celico v tabeli k objektu.
     * @param id seznam, id[0].. x, id[1]..y koordinata
     * @returns {boolean}
     */
    smemIzbrisati(x,y){
        if (this.getSteviloElementov() === 1) {
            return true;
        }

        return preveriObjektZaIzbris(x,y);
    }

    /**
     * Metoda določi število sosedov elementa na koordinati x in y
     * @param x
     * @param y
     * @returns {number}  stevilo sosednjih elementov
     */
    getSteviloSosedov(x,y){
        var stSosedov = 0;

        if (x + 1 + ZAMIKV < VRSTICE && this.dobiElement(x + 1,y) != null) {
            stSosedov = stSosedov + 1;
        }
        if (x - 1 + ZAMIKV >= 0 && this.dobiElement(x - 1,y) != null) {
            stSosedov = stSosedov + 1;
        }
        if (y + 1 + ZAMIKS < STOLPCI && this.dobiElement(x,y + 1) != null) {
            stSosedov = stSosedov + 1;
        }
        if (y - 1 + ZAMIKS >= 0 && this.dobiElement(x ,y - 1) != null) {
            stSosedov = stSosedov + 1;
        }
        return stSosedov;
    }


    getSteviloElementov(){
        return this.elementi.length;
    }

    getBarvaFasade(){
        return this.barvaFasada;
    }

    setBarvaFasade(barva){
        this.barvaFasada = barva;
    }

    getBarvaStreha(){
        return this.barvaStreha;
    }

    setBarvaStrehe(barva){
        this.barvaStreha = barva;
    }


    posodobiStreho(x,y){
        if (x + 1 + ZAMIKS < STOLPCI && this.dobiElement(x + 1,y) != null) {
            this.dobiElement(x,y).nastaviStreha(this.dobiElement(x + 1,y).dobiStreha());
            return;
        }
        if (x - 1 + ZAMIKS >= 0 && this.dobiElement(x - 1 ,y ) != null) {
            this.dobiElement(x,y).nastaviStreha(this.dobiElement(x - 1,y).dobiStreha());
            return;
        }
    }

    /**
     * Metoda pregleda vse elemente in določi njihove lastnosti.
     */
    posodobiElemente(){
        for (var index = 0; index < this.elementi.length; index++){

            var stRobnihSosedov = 0;
            var x = this.elementi[index].pozicijaX;
            var y = this.elementi[index].pozicijaY;

            //TODO je to prav da so stolpci?
            if (y + 1 + ZAMIKS < STOLPCI && this.dobiElement(x,y + 1) != null) {
               stRobnihSosedov++;
            }
            if (y - 1 + ZAMIKS >= 0 && this.dobiElement(x ,y - 1) != null) {
               stRobnihSosedov++;
            }

            var stevec = 0
            for (var i = 0; i < this.elementi.length; i++){ //najprej poiscem vse rastre, ki so v y vrsti
                if (this.elementi[i].pozicijaY === y){
                    stevec++;
                }
            }

            this.elementi[index].zakljucni = false;
            if (this.elementi[index].tip === "B" || stevec === 1){
                this.elementi[index].zakljucni = true;
            }

            this.elementi[index].robni = true;
            if (stRobnihSosedov > 1){
                this.elementi[index].robni = false;
            }
        }
    }

    /**
     * metoda posodobi samo sosednje elemente in elemente v celotnem stolpcu
     * @param x
     * @param y
     * @param dodajanje  ali smo element dodali (true). Privzeto je, da smo ga odstranili
     */
    posodobiStene(x,y,dodajanje = false){
        if (x + 1 + ZAMIKV < VRSTICE && this.dobiElement(x + 1,y) != null) {
            if (dodajanje) {
                this.dobiElement(x, y).dobiSteno(2).setVidna(false);
                this.dobiElement(x + 1, y).dobiSteno(3).setVidna(false);
                //izbrisem se plosce
                if (MODEL.jeAktiven())
                    MODEL.dobiElement(x+1,y).stene[3].odstraniOdprtine();
            }
            else {
                this.dobiElement(x + 1, y).dobiSteno(3).setVidna(true);
                if (this.dobiElement(x + 1, y).dobiTipElementa() === "B"){
                    prikazOdstraniOdprtinaElement(x + 1, y, 0 , 0 );
                    prikazOdstraniOdprtinaElement(x + 1, y, 1 , 0 );
                }
                for (var l = 0; l < 3; l++) {
                    ustvariPlosceCelna(x + 1, y, 3, l, MODEL.dobiElement(x + 1, y));
                }
            }
        }
        if (x - 1 + ZAMIKV >= 0 && this.dobiElement(x - 1,y) != null) {
            if (dodajanje) {
                this.dobiElement(x, y).dobiSteno(3).setVidna(false);
                this.dobiElement(x - 1, y).dobiSteno(2).setVidna(false);
                if (MODEL.jeAktiven())
                    MODEL.dobiElement(x-1,y).stene[2].odstraniOdprtine();
            }
            else {
                this.dobiElement(x - 1, y).dobiSteno(2).setVidna(true);
                if (this.dobiElement(x - 1, y).dobiTipElementa() === "B"){
                    prikazOdstraniOdprtinaElement(x - 1, y, 0 , 0 );
                    prikazOdstraniOdprtinaElement(x - 1, y, 1 , 0 );
                }
                for (var l = 0; l < 3; l++) {
                    ustvariPlosceCelna(x - 1, y, 2, l, MODEL.dobiElement(x - 1, y));
                }
            }
        }
        if (y + 1 + ZAMIKS < STOLPCI && this.dobiElement(x,y + 1) != null) {
            if (dodajanje) {
                (this.dobiElement(x,y).dobiTipElementa() === "A") ? this.dobiElement(x, y).dobiSteno(0).setVidna(true) : this.dobiElement(x, y).dobiSteno(0).setVidna(false);
                if (this.dobiElement(x,y + 1).dobiTipElementa() === "B" || this.dobiElement(x,y + 1).dobiTipElementa() === "C") {
                    this.dobiElement(x, y + 1).stene[1] = new Stena(0);
                    this.dobiElement(x, y + 1).dobiSteno(1).setVidna(false);
                }
                if (MODEL.jeAktiven())
                    MODEL.dobiElement(x,y + 1).stene[1].odstraniOdprtine();
            }
            else {
                this.dobiElement(x, y + 1).dobiSteno(1).setVidna(true);
                ustvariPlosceKapna(x,y + 1,1,MODEL.dobiElement(x,y + 1));
            }
        }
        if (y - 1 + ZAMIKS >= 0 && this.dobiElement(x ,y - 1) != null) {
            if (dodajanje) {
                (this.dobiElement(x,y).dobiTipElementa() === "A") ? this.dobiElement(x, y).dobiSteno(1).setVidna(true) : this.dobiElement(x, y).dobiSteno(1).setVidna(false);

                if (this.dobiElement(x,y - 1).dobiTipElementa() === "B" || this.dobiElement(x,y - 1).dobiTipElementa() === "C") {
                    this.dobiElement(x, y - 1).stene[0] = new Stena(0);
                    this.dobiElement(x, y - 1).dobiSteno(0).setVidna(false);
                }
                if (MODEL.jeAktiven())
                    MODEL.dobiElement(x,y - 1).stene[0].odstraniOdprtine();
            }
            else {
                this.dobiElement(x, y - 1).dobiSteno(0).setVidna(true);
                ustvariPlosceKapna(x,y - 1,0,MODEL.dobiElement(x,y - 1));
            }
        }

        for (var index = 0; index < this.elementi.length; index++) { //
            if (this.elementi[index].pozicijaY === y){
                if (this.elementi[index].dobiTipElementa()==="A"){
                    this.elementi[index].dobiSteno(0).setVidna(true);
                    this.elementi[index].dobiSteno(1).setVidna(true);
                }
                else {
                    if (y + 1 + ZAMIKS < STOLPCI && this.dobiElement(this.elementi[index].pozicijaX,y + 1) != null) {  //popravi samo trenutnega
                        this.elementi[index].dobiSteno(0).setVidna(false);
                    }
                    if (y - 1 + ZAMIKS >= 0 && this.dobiElement(this.elementi[index].pozicijaX ,y - 1) != null) {
                        this.elementi[index].dobiSteno(1).setVidna(false);
                    }
                }
            }
        }

    }

    dolociTipRastrov(x,y){
        var vrsta = [];
        for (var index = 0; index < this.elementi.length; index++){ //najprej poiscem vse rastre, ki so v y vrsti
            if (this.elementi[index].pozicijaY == y && this.elementi[index].oblika === 1){
                vrsta.push(this.elementi[index]);
            }
        }
        var test = true; //sortiranje - bubble sort
        while (test){
            test = false;
            for (let i = 0; i < vrsta.length - 1; i++){
                if (vrsta[i].pozicijaX > vrsta[i+1].pozicijaX){
                    var temp = vrsta[i];
                    vrsta[i] = vrsta[i+1];
                    vrsta[i+1] = temp;
                    test = true;
                }
            }
        }

        if (vrsta.length > 0) {
            vrsta[0].tip = 'A';  //prvi je vedno A
            if (vrsta.length > 1) {
                vrsta.push(new Element(200,y,1)); // dodam stražarja, da pravilno označi
                for (let i = 1; i < vrsta.length; i++) {  //vmesni so C
                    vrsta[i].tip = 'C';
                    if (vrsta[i].pozicijaX !== vrsta[i-1].pozicijaX + 1){
                        vrsta[i].tip = 'A';
                    }
                    else if (i + 1 < vrsta.length && vrsta[i].pozicijaX+1 !== vrsta[i+1].pozicijaX){
                        vrsta[i].tip = 'B';
                    }
                }
            }
        }


    }
}

/**
 * Razred za osnovne elemente (rastre) hiše.
 *
 * seznam sten [ 0 desno,1 levo, 2 spredaj, 3 zadaj, 4 zgoraj, 5 spodaj]
 * oblika : 1 raster, 2 balkon, 3 terasa
 */
class Element{
    constructor(pozicijaX, pozicijaY, oblika = 1){
        this.pozicijaX = pozicijaX;
        this.pozicijaY = pozicijaY;
        this.tip = 'A';
        if (oblika === 2){  //balkon -- stranske stene
            this.tip = 'D';
        } else if (oblika === 3) { //terasa, samo tla in strop + stebra
            this.tip = 'E';
        }
        this.stene = [new Stena(0),new Stena(1),new Stena(2),new Stena(3),new Stena(4),new Stena(5)];
        this.streha = 0; // 0 - ravna, 1 - dvokapna
        this.zakljucni = false;
        this.robni = false;
        this.oblika = oblika;
        this.orientacija = 1; // orientacija pove ali se dodaja spredaj ali zadaj v vrsto, pomembno predvsem pri stebrih. moznosti so 1 (spredaj) in -1 (zadaj)
    }

    dobiSteno(x){
        return this.stene[x];
    }

    dobiStreha(){
        return this.streha
    }

    nastaviStreha(x){
        this.streha = x;
    }

    dobiOblikoElementa(){
        return this.oblika;
    }

    nastaviTipElementa(tip){
        this.tip = tip;
    }

    dobiTipElementa(){
        return this.tip;
    }

    /*
    vrne Boolean ali je stena vidna ali ne. Kot vhod je lahko stevilo ali opisno
    stevilo je med 0 in 5
     */
    jeStenaVidna(stena){
        if (Number.isInteger(stena)){
            if (stena >= 0 && stena <= 5) {
                return this.stene[stena].jeVidna();
            }
            return false;
        }
        else if (stena === "desno"){
            return this.stene[0].jeVidna();
        }
        else if (stena === "levo"){
            return this.stene[1].jeVidna();
        }
        else if (stena === "spredaj"){
            return this.stene[2].jeVidna();
        }
        else if (stena === "zadaj"){
            return this.stene[3].jeVidna();
        }
        return false;
    }
}

/**
 * Objekt za stene
 * vidna .. je stena vidna v osnovnem prikazu
 * okno .. ima okno
 * okno_img .. datoteka za okno
 * vrata .. ima vrata
 * vrata_img .. datoteka za prikaz vrat
 * notranja .. je notranja vmesna stena, vidna mora biti v tem primeru false
 *
 */
class Stena{
    constructor(pozicija) {
        this.pozicija = pozicija;
        this.vidna = true;
        this.odprtina = [new Odprtina(0),new Odprtina(1), new Odprtina(2)];
    }

    setVidna(x){
        this.vidna = x;
    }

    jeVidna(){
        return this.vidna;
    }

    jeKapna(){
        if (this.pozicija == 0 || this.pozicija == 1)
            return true;
        return false;
    }

    jeCelna(){
        if (this.pozicija == 2 || this.pozicija == 3)
            return true;
        return false;
    }
}

class Odprtina {
    constructor(pozicija) {
        this.obstaja = false;
        this.id = null;
        this.pozicija = pozicija;
        this.zacetna = false; // podatek pove ali se odprtina zacne na tej poziciji.
        this.povezava = []; // seznam odprtin, ki so povezane, uporabno pri dveh in treh širinah.
    }

    nastaviOdprtino(id){
        this.id = id;
        this.obstaja = true;
    }

    odstraniOdprtino(){
        this.obstaja = false;
        this.id = null;
        this.pozicija = null;
        this.povezava = [];
        this.zacetna = false;
    }

    dodajPovezavo(x){
        this.povezava.push(x);
    }

}

/**
 * Vsi razredi oblike ModelXYZ služijo za shranjevanje 3D objektov. Torej, da
 */

class ModelObjekt {
    constructor() {
        this.elementi = [];
        this.oseba;
        //skupne mere
        this.meraD = null;
        this.puscicaD1 = null;
        this.puscicaD2 = null;
        this.meraS = null;
        this.puscicaS1 = null;
        this.puscicaS2 = null;
    };

    dobiElement(x,y){
        for (var index = 0; index < this.elementi.length; index++){
            if (this.elementi[index].pozicijaX == x && this.elementi[index].pozicijaY == y)
                return this.elementi[index];
        }
        return null;
    }

    jeAktiven(){
        return this.elementi.length > 0;
    }
}

class ModelElement {
    constructor(pozicijaX, pozicijaY) {
        this.pozicijaX = pozicijaX;
        this.pozicijaY = pozicijaY;
        this.stene = [new ModelStena(),new ModelStena(),new ModelStena(),new ModelStena()];
        this.streha = null;
        this.strehaCrteSpredaj = null;
        this.strehaCrteZadaj = null;
        this.kocka = null;
        this.meraD = null;
        this.puscicaD1 = null;
        this.puscicaD2 = null;
        this.meraS = null;
        this.puscicaS1 = null;
        this.puscicaS2 = null;
        this.hCrtaSpodaj = null;
        this.hCrtaZgoraj = null;
        this.tipDesno = null;
        this.tipLevo = null;
        this.terasa = [];
    };
}

class ModelStena {
    constructor() {
        this.odprtina = [new ModelOdprtina(),new ModelOdprtina(), new ModelOdprtina()];
    };

    odstraniOdprtine(){
        for (var i = 0; i < 3; i++) {
            SCENA.remove(this.odprtina[i].ravnina);
            this.odprtina[i].ravnina = null;
            this.odprtina[i].crte = null;
        }
    }
}

class ModelOdprtina{
    constructor() {
        this.ravnina = null;
        this.crte = null;
    }
}