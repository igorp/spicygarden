var CENE = [2000, 600, 250, 500];

var ENOJNA = [1,2,3,4];

//globalne spremenljivke
var OBJEKT = new Objekt();  // objekni model hise. Shranjeno vse o tej hisi
var MODEL = new ModelObjekt(); // v njem so shranjeni vsi 3D elementi, ki so dodani na sceno
var SCENA, camera, controls, renderer;
var WIREFRAME;
var LOADER = new THREE.TextureLoader();
var CLOCK = new THREE.Clock();
var OSEBA;
var OSEBA_NALOZENA = false;

// velikost elementa
var DOLZINA = 406;
var VISINA = 320;
var SIRINA = 122;
var OSEBA_VISINA = 100;
var TERASA_SIRINA = 20;

var STEBER_SIRINA = 6;
var STEBER_DOLZINA = 15;

// velikost mreže
var VRSTICE = 50;
var STOLPCI = 20;

// ZAMIK pove za koliko stevilcno premaknemo glede na 3D svet (tam je sredina na izhodišče).
var ZAMIKV = VRSTICE/2;
var ZAMIKS = STOLPCI/2;

var CONTAINER;

// Tloris dimenzije
var TLORIS_SIRINA = 122;
var TLORIS_DOLZINA = 406;
var TLORIS_STEBER_V = 20; //visina
var TLORIS_STEBER_S = 8;  //sirina
var TLORIS_ZAMIK = 2;
var TLORIS_ODPRTINA = TLORIS_SIRINA - TLORIS_STEBER_S - 2 * TLORIS_ZAMIK;
var TLORIS_PADDING = 70;

var SRAFURA_POT = "images/srafura.png";
var SRAFURA_SLIKA;

// Naris dimenzije
var NARIS_SIRINA = 416;
var NARIS_STEBER_VISINA = 278;
let NARIS_STEBER_SIRINA = 25;
let NARIS_VRATA = 220;
var NARIS_STREHA = 178;
var NARIS_ODPRTINA_SIRINA = 2;