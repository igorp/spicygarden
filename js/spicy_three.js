/**
 Avtor: Igor Pesek
 Lastnik: Objem narave, d.o.o.
 Avtorske pravice zadržane, 2020
 Licence: Three.js
 Three.interaction.js
 Jquery.js
 **/


function pripraviPodlago(scena){

    for (i = 0; i <= VRSTICE; i++){
        for (j = 0; j <= STOLPCI; j++){

            //pripravimo en raster
            var rasterGeometrija = new THREE.PlaneGeometry(DOLZINA, SIRINA);
            rasterGeometrija.rotateX(Math.PI / 2);
            rasterGeometrija.rotateY(Math.PI /2);
            rasterGeometrija.rotateZ(Math.PI);
            var ravninaMaterial = new THREE.MeshBasicMaterial({
                color: 0xffff00,
                side: THREE.DoubleSide,
                transparent: true,
                opacity: 0
            });

            var rasterMesh = new THREE.Mesh(rasterGeometrija, ravninaMaterial);
            //nastavim pozicijo
            rasterMesh.position.set((i-ZAMIKV) * SIRINA,-VISINA/2+2,-(j-ZAMIKS) * DOLZINA);

            rasterMesh.name = "podlaga_" + (i-ZAMIKV).toString() + "_" + (j-ZAMIKS).toString();

            //okvir
            //okvir
            var geometrija = new THREE.EdgesGeometry( rasterMesh.geometry );
            var crtaMaterial = new THREE.LineBasicMaterial( { color: 0xA9A9A9, linewidth: 1 } );
            var wireframe = new THREE.LineSegments( geometrija, crtaMaterial );
            wireframe.renderOrder = 2; // make sure wireframes are rendered 2nd
            rasterMesh.add( wireframe );

            //Interaktivnost
            rasterMesh.cursor = 'pointer';
            rasterMesh.on('mousedown', function (ev) {
                MOUSEDOWN = true;
                MOUSEMOVE = false;
            });

            rasterMesh.on('mousemove', function (ev) {
                if (MOUSEDOWN) {
                    MOUSEMOVE = true;
                }
            });

            rasterMesh.on('mouseup', function (ev) {
                // MOUSEMOVE potreben, da lahko vrtimo, a ne dodamo elementa
                if (MOUSEMOVE) {
                    MOUSEDOWN = false;
                    MOUSEDOWN = false;
                } else {
                    var id = ev.data.target.name.split("_");
                    manipuliranjeElement(parseInt(id[1]),parseInt(id[2]));
                }
            });

            rasterMesh.on('touchend', function (ev) {
                var id = ev.data.target.name.split("_");
                manipuliranjeElement(parseInt(id[1]),parseInt(id[2]));
            });

            scena.add(rasterMesh);

        }
    }


}

/**
 * metoda odprtino na elementu in ustvari prazne plošče
 * @param x
 * @param y
 * @param k
 * @param l
 */
var prikazOdstraniOdprtinaElement = function (x,y,k,l){
    prikazOdstraniOdprtine(OBJEKT.dobiElement(x,y),MODEL.dobiElement(x,y),k,l);
    if (k < 2){ // delamo kapne
        ustvariPlosceKapna(x,y,k,MODEL.dobiElement(x,y));
    }
    else {
        ustvariPlosceCelna(x,y,k,l,MODEL.dobiElement(x,y))
    }

}


var prikazOdstraniOdprtine = function (element, mElement, k, l){
    SCENA.remove(mElement.stene[k].odprtina[l].ravnina);
    var povezane = element.stene[k].odprtina[l].povezava;
    element.stene[k].odprtina[l].odstraniOdprtino();
    while (povezane.length>0){
        l = parseInt(povezane.pop());
        SCENA.remove(mElement.stene[k].odprtina[l].ravnina);
        element.stene[k].odprtina[l].odstraniOdprtino();
        ustvariPlosceCelna(element.pozicijaX, element.pozicijaY, k, l, mElement);
    }
}

var dodajDogodkeNaObjekt = function(obj){
    //Interaktivnost
    obj.cursor = 'pointer';

    obj.on('click', function (ev) {
        dogodekKlikRaster(ev);
    });

    obj.on('touchend', function (ev) {
        dogodekKlikRaster(ev);
    });
}


var prikazPloscaOdprtina = function (x, y, k, l, dolzina, element, mElement, podatekID){
    var extensions = [[""],["l", "d"], ["l","s","d"]];
    var rotacija = [Math.PI, 0, Math.PI/2, -Math.PI/2];
    var extension = extensions[dolzina - 1];

    element.stene[k].odprtina[l].zacetna = true;  //tu se zacne odprtina

    for (var i = 0; i < dolzina; i++) {
        var src = PODATKI[podatekID][0];
        src = src.spicy_insert(extension[i]);
        element.stene[k].odprtina[i + l].nastaviOdprtino(podatekID);

        var ravninaGeo = new THREE.PlaneGeometry(SIRINA, VISINA);

        ravninaGeo.rotateY(rotacija[k]);
        var tekstura = LOADER.load(src);
        var ravninaMat = new THREE.MeshBasicMaterial({
            side: THREE.DoubleSide,
            map: tekstura,
            transparent: true,
            alphaTest: 0.5
        });
        var plosca = new THREE.Mesh(ravninaGeo, ravninaMat);

        if (k == 2) {  //spredaj - l narasca
            plosca.position.set(posX(x, y, k), 1, posZ(x, y, k) - (i + l - 1) * SIRINA);
            plosca.name = x.toString() + "_" + y.toString() + "_" + k.toString() + "_" + (i + l).toString();
        }
        else if (k == 3) { //zadaj - l pada
            plosca.position.set(posX(x, y, k), 1, posZ(x, y, k) + (i + l - 1) * SIRINA);
            plosca.name = x.toString() + "_" + y.toString() + "_" + k.toString() + "_" + (i + l).toString();
        }
        else { // kapne
            plosca.position.set(posX(x, y, k), 1, posZ(x, y, k));
            plosca.name = x.toString() + "_" + y.toString() + "_" + k.toString() + "_0";
        }

        //dodamo crte na celne ploskve
        var crte = pripraviRoboveGeometrije(plosca.geometry);
        plosca.add(crte);
        if (!$("#prikaziCrte").is(':checked')) {
            crte.visible = false;
        }

        //Interaktivnost
        plosca.cursor = 'pointer';

        plosca.on('click', function (ev) {
            dogodekKlikStena(ev);
        });

        plosca.on('touchend', function (ev) {
            dogodekKlikStena(ev);
        });
        SCENA.add(plosca);
        mElement.stene[k].odprtina[i + l].ravnina = plosca;
        mElement.stene[k].odprtina[i+ l].crte = crte;
    }
}



var dogodekKlikStena = function (ev) {
    var pozicija = $.map(ev.data.target.name.split('_'), function (value) {
        return parseInt(value, 10);
    });
    var x = pozicija[0]; y = pozicija[1]; k = pozicija[2]; l = pozicija[3];
    // pozicija <= [x, y, k, l]  k je stena, l je odprtina od leve proti desni 0,1,2
    var element = OBJEKT.dobiElement(pozicija[0],pozicija[1]);
    var mElement = MODEL.dobiElement(pozicija[0],pozicija[1]);

    // ko je stena kliknjena
    if ($(".imgFocus").length == 1) {//nekaj je izbrano med dodatki

        if ($(".imgFocus").first().hasClass("streha")){
            if ($(".imgFocus").first().hasClass("ravna")){
                spremeniStreho(x,y,0);
            }
            else {
                spremeniStreho(x,y,1);
            }
        }
        else if ($(".imgFocus").first().hasClass("kos")) { //brisemo vse
            if (!element.stene[k].odprtina[l].obstaja){
                manipuliranjeElement(x,y);
            }
            //dodam prazne plošče
            else if (element.stene[k].jeKapna()) {
                prikazOdstraniOdprtine(element,mElement,k,l);
                ustvariPlosceKapna(x,y,k,mElement);
            }
            else {  //je celna
                var povezane = element.stene[k].odprtina[l].povezava;
                prikazOdstraniOdprtine(element, mElement, k, l);
                ustvariPlosceCelna(x,y,k,l,mElement);
                while (povezane.length>0){
                    l = parseInt(povezane.pop());
                    prikazOdstraniOdprtine(element, mElement, k, l);
                    ustvariPlosceCelna(x,y,k,l,mElement);
                }
            }
            shraniTlorisKotSliko();
        }
        else if ($(".imgFocus").first().hasClass("okno")){
            var podatekID = parseInt($(".imgFocus").first().attr("id").split("_")[1]);
            if (element.tip !== "C" && element.stene[k].jeKapna()){
                alert("v rob ne smemo dodajati");
            }
            else if (element.stene[k].jeKapna() &&  $.inArray(podatekID,ENOJNA) !== -1){

                prikazOdstraniOdprtine(element, mElement, k, 0);
                prikazPloscaOdprtina(x, y, k , l ,1, element, mElement, podatekID);

            }
            else if (element.stene[k].jeCelna()){

                if (dolociDolzinoOdprtine(podatekID) == 1){ //enojna odprtina

                    prikazOdstraniOdprtine(element, mElement, k, l);
                    prikazPloscaOdprtina(x, y, k ,l ,1, element, mElement, podatekID);
                }
                else if (dolociDolzinoOdprtine(podatekID) == 2){ //"dvojka"

                    var pozicijaLevo = l;
                    if (pozicijaLevo == 2) pozicijaLevo = 1;

                    // če je bila povezana, potem je potrebno staro izbrisati
                    prikazOdstraniOdprtine(element, mElement, k, pozicijaLevo + 1);
                    prikazOdstraniOdprtine(element, mElement, k, pozicijaLevo);


                    prikazPloscaOdprtina(x, y, k ,pozicijaLevo ,2, element, mElement, podatekID);

                    element.stene[k].odprtina[pozicijaLevo].dodajPovezavo(pozicijaLevo + 1);
                    element.stene[k].odprtina[pozicijaLevo + 1].dodajPovezavo(pozicijaLevo);

                }
                else {   //"trojka"
                    var pozicijaLevo = 0;
                    prikazOdstraniOdprtine(element, mElement, k, 0);
                    prikazOdstraniOdprtine(element, mElement, k, 1);
                    prikazOdstraniOdprtine(element, mElement, k, 2);

                    prikazPloscaOdprtina(x, y, k ,pozicijaLevo ,3, element, mElement, podatekID);

                    element.stene[k].odprtina[0].povezava = [1,2];
                    element.stene[k].odprtina[1].povezava = [0,2];
                    element.stene[k].odprtina[2].povezava = [0,1];
                }
            }
            // izbrisem izbiro
            shraniTlorisKotSliko();
        }

        var cena = dolociCeno();
        $("#cena_objekta").html(cena + " €");

    }
}

var dogodekKlikRaster = function (ev) {
    var pozicija = $.map(ev.data.target.name.split('_'), function (value) {
        return parseInt(value, 10);
    });
    var x = pozicija[0]; y = pozicija[1];

    // ko je nekaj izbrano
    if ($(".imgFocus").length == 1) {

        if ($(".imgFocus").first().hasClass("streha")){
            if ($(".imgFocus").first().hasClass("ravna")){
                spremeniStreho(x,y,0);
            }
            else {
                spremeniStreho(x,y,1);
            }

        }
        else if ($(".imgFocus").first().hasClass("kos")) { //brisemo vse
            //dodam prazne plošče
            manipuliranjeElement(x,y);
        }

        var cena = dolociCeno();
        $("#cena_objekta").html(cena+ " €");

    }

}


function prikazDodajElement(x,y){
    //priprava za 3D modele
    var mElement = new ModelElement(x,y);
    var element = OBJEKT.dobiElement(x,y);

    MODEL.elementi.push(mElement);

    // ustvarim osnovno kocko
    ustvariKocko(x,y,mElement);

    //dodamo še plošče za odprtine
    for (var k = 0; k < 4; k++) {
        if (element.stene[k].jeVidna()) {
            if (element.stene[k].jeKapna()) {
                //dodajStranskiTip(x,y,k,mElement);
                if (element.stene[k].odprtina[0].obstaja){
                    prikazPloscaOdprtina(x,y,k,0,1,element,mElement,element.stene[k].odprtina[0].id);
                }
                else {
                    ustvariPlosceKapna(x, y, k, mElement);
                }
            }
            if (element.stene[k].jeCelna()) {
                for (var l = 0; l < 3; l++) {
                    if (element.stene[k].odprtina[l].obstaja){
                        if (element.stene[k].odprtina[l].zacetna)
                            prikazPloscaOdprtina(x,y,k,l,dolociDolzinoOdprtine(element.stene[k].odprtina[l].id),element,mElement,element.stene[k].odprtina[l].id);
                    }
                    else {
                        ustvariPlosceCelna(x, y, k, l, mElement);
                    }
                }
            }
        }
    }

    //dodam streho
    if (OBJEKT.dobiElement(x,y).dobiStreha() === 0){
        var streha = pripraviRavnoStreho(x,y);
    } else{
        var streha = pripraviDvokapnaStreha(x,y);
        mElement.strehaCrteSpredaj = narisiCrteStreha(x,y,1);
        mElement.strehaCrteZadaj = narisiCrteStreha(x,y,2);
        if (!$("#prikaziCrte").is(':checked')){
            mElement.strehaCrteSpredaj.visible = false;
            mElement.strehaCrteZadaj.visible = false;
        }
    }
    SCENA.add(streha);
    mElement.streha = streha;

    // mere prikazem samo v primeru, če je odkljukano
    if ($("#prikaziMere").is(':checked') && OBJEKT.elementi.length === MODEL.elementi.length) {
        pripraviMereObjekt();
    }
}

var dodajStranskiTip = function(x,y,k,mElement){

    var zamik = -1;
    var rotacija = Math.PI;
    var visina = 0;
    if (k === 1){
        zamik = 1;
        rotacija = 0;
        visina = VISINA;
    }

    var ravninaGeo = new THREE.PlaneGeometry(SIRINA-6, 3 * TERASA_SIRINA-8);
    ravninaGeo.rotateY(rotacija);

    var canvas1 = document.createElement('canvas')
    canvas1.setAttribute('width', SIRINA-6);
    canvas1.setAttribute('height', 3 * TERASA_SIRINA-8);
    var context1 = canvas1.getContext('2d');
    context1.font = "24px Roboto Mono";
    context1.fillStyle = "rgba(255,255,255,0.95)";
    context1.fillText(OBJEKT.dobiElement(x,y).dobiTipElementa(), (SIRINA-6)/2-10, (3 * TERASA_SIRINA-8)/2+5);
    var texture1 = new THREE.Texture(canvas1);
    texture1.needsUpdate = true;


    var ravninaMat = new THREE.MeshBasicMaterial({map: texture1, side:THREE.DoubleSide, transparent: true });

    var plosca = new THREE.Mesh(ravninaGeo, ravninaMat);
    plosca.position.set(posX(x,y,k), VISINA/2 - 3/2 * TERASA_SIRINA-2, posZ(x,y,k) + zamik);
    plosca.name = x.toString() + "_" + y.toString() + "_" + k.toString() + "_0";

    if (k === 0){
        mElement.tipDesno = plosca;
    }
    else {
        mElement.tipLevo = plosca;
    }

    SCENA.add(plosca);
}


/**
 * Metoda ustvari plosco na kapni strani. Plosca se odzove na klik.
 * @param x koordinata X
 * @param y koordinata Y
 * @param k stena
 * @param mElement  modelElement
 */
var ustvariPlosceKapna = function (x, y, k, mElement){
    var ravninaGeo = new THREE.PlaneGeometry(SIRINA-6, VISINA-6);

    var ravninaMat = new THREE.MeshBasicMaterial({
        color: 0xff00ff,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0
    });

    var plosca = new THREE.Mesh(ravninaGeo, ravninaMat);
    plosca.position.set(posX(x,y,k), 1, posZ(x,y,k));
    plosca.name = x.toString() + "_" + y.toString() + "_" + k.toString() + "_0";

    //Interaktivnost
    plosca.cursor = 'pointer';

    plosca.on('click', function (ev) {
        dogodekKlikStena(ev);
    });

    plosca.on('touchend', function (ev) {
        dogodekKlikStena(ev);
    });
    SCENA.add(plosca);
    mElement.stene[k].odprtina[0].ravnina = plosca;
}

/**
 * Metoda ustvari plosco na celni strani. Plosca se odzove na klik.
 * @param x koordinata X
 * @param y koordinata Y
 * @param k stena
 * @param l zaporedna številka plošče
 * @param mElement  modelElement
 */
var ustvariPlosceCelna = function (x, y, k, l, mElement){
    var ravninaGeo = new THREE.PlaneGeometry(SIRINA, VISINA);

    ravninaGeo.rotateY(Math.PI / 2);

    var ravninaMat = new THREE.MeshBasicMaterial({
        color: 0xff0000,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0
    });


    var plosca = new THREE.Mesh(ravninaGeo, ravninaMat);
    if (k == 2) {
        plosca.position.set(posX(x, y, k), 0, posZ(x, y, k) - (l - 1) * SIRINA);
    }
    else{
        plosca.position.set(posX(x, y, k), 0, posZ(x, y, k) + (l - 1) * SIRINA);
    }
    plosca.name = x.toString() + "_" + y.toString() + "_" + k.toString() + "_" + l.toString();

    //dodamo crte na celne ploskve
    var crte = pripraviRoboveGeometrije(plosca.geometry);
    plosca.add(crte);
    if (!$("#prikaziCrte").is(':checked')) {
        crte.visible = false;
    }
    //Interaktivnost
    plosca.cursor = 'pointer';



    plosca.on('click', function (ev) {
        let cl = CLOCK.getDelta();
        if ($(".imgFocus").length == 1){
            dogodekKlikStena(ev);
        }
        else if (cl > 1)
            dogodekKlikStena(ev);
    });

    plosca.on('touchend', function (ev) {
        dogodekKlikStena(ev);
    });
    SCENA.add(plosca);
    mElement.stene[k].odprtina[l].ravnina = plosca;
    mElement.stene[k].odprtina[l].crte = crte;
}

/**
 * Metoda ustvari osnovno kocko-raster
 * @param x koordinata X
 * @param y koordinata Y
 * @param mElement modelElement na tej koordinati
 */
var ustvariKocko = function (x, y, mElement){

    var fasadaMaterial = new THREE.MeshStandardMaterial({color: OBJEKT.getBarvaFasade()});
    var kockaGeometry = new THREE.BoxBufferGeometry(DOLZINA, VISINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    var kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,0,- y * DOLZINA);
    kocka.name = x.toString() + "_" + y.toString();

    SCENA.add(kocka);

    kocka.add( pripraviRoboveGeometrije(kocka.geometry) );

    kocka.cursor = 'pointer';
    kocka.on('mousedown', function (ev) {
        MOUSEDOWN = true;
        MOUSEMOVE = false;
    });

    kocka.on('mousemove', function (ev) {
        if (MOUSEDOWN) {
            MOUSEMOVE = true;
        }
    });

    kocka.on('mouseup', function (ev) {
        // MOUSEMOVE potreben, da lahko vrtimo, a ne dodamo elementa
        if (MOUSEMOVE) {
            MOUSEDOWN = false;
            MOUSEDOWN = false;
        } else {
            var pozicija = $.map(ev.data.target.name.split('_'), function(value){
                return parseInt(value, 10);
            });
            manipuliranjeElement(pozicija[0],pozicija[1]);
        }
    });
    mElement.kocka = kocka;
    mElement.hCrtaSpodaj = narisiHorizontalnoCrto(x,y,1);
    mElement.hCrtaZgoraj = narisiHorizontalnoCrto(x,y,2);
}


/**
 * Metoda ustvari teraso
 * @param x koordinata X
 * @param y koordinata Y
 * @param mElement modelElement na tej koordinati
 */
var ustvariBalkon = function (x, y){

    var mElement = new ModelElement(x,y);

    MODEL.elementi.push(mElement);

    var fasadaMaterial = new THREE.MeshStandardMaterial({color: OBJEKT.getBarvaFasade()});
    var kockaGeometry = new THREE.BoxBufferGeometry(TERASA_SIRINA, VISINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    var kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,0,- (y * DOLZINA) + (DOLZINA-TERASA_SIRINA)/2 );
    //console.log (x," ",y," ",x * SIRINA, " ", - y * DOLZINA);
    kocka.name = x.toString() + "_" + y.toString();
    dodajDogodkeNaObjekt(kocka);
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    // kocka.add( pripraviRoboveGeometrije(kocka.geometry) );


    kockaGeometry = new THREE.BoxBufferGeometry(TERASA_SIRINA, VISINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,0,- (y * DOLZINA) - (DOLZINA-TERASA_SIRINA)/2 );
    kocka.name = x.toString() + "_" + y.toString();
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    // kocka.add( pripraviRoboveGeometrije(kocka.geometry) );
    dodajDogodkeNaObjekt(kocka);


    kockaGeometry = new THREE.BoxBufferGeometry(DOLZINA - 2*TERASA_SIRINA,   TERASA_SIRINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,-VISINA/2 + TERASA_SIRINA*0.5,- (y * DOLZINA) );
    kocka.name = x.toString() + "_" + y.toString();
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    // kocka.add( pripraviRoboveGeometrije(kocka.geometry) );
    dodajDogodkeNaObjekt(kocka);

    kockaGeometry = new THREE.BoxBufferGeometry(DOLZINA - 2*TERASA_SIRINA,   3*TERASA_SIRINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,VISINA/2 - TERASA_SIRINA*1.5,- (y * DOLZINA) );
    kocka.name = x.toString() + "_" + y.toString();
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    // kocka.add( pripraviRoboveGeometrije(kocka.geometry) );
    dodajDogodkeNaObjekt(kocka);

    mElement.hCrtaSpodaj = narisiHorizontalnoCrto(x,y,1);
    mElement.hCrtaZgoraj = narisiHorizontalnoCrto(x,y,2);

    mElement.terasa.push(narisiCrteTerasaObod(x,y,1));
    mElement.terasa.push(narisiCrteTerasaObod(x,y,2));
    mElement.terasa.push(narisiCrteTerasaVmesne(x,y,1));
    mElement.terasa.push(narisiCrteTerasaVmesne(x,y,2));
    mElement.terasa.push(narisiCrteTerasaVmesne(x,y,3));
    mElement.terasa.push(narisiCrteTerasaVmesne(x,y,4));


    //dodam streho
    if (OBJEKT.dobiElement(x,y).dobiStreha() === 0){
        var streha = pripraviRavnoStreho(x,y);
    } else{
        var streha = pripraviDvokapnaStreha(x,y);
        mElement.strehaCrteSpredaj = narisiCrteStreha(x,y,1);
        mElement.strehaCrteZadaj = narisiCrteStreha(x,y,2);
        if (!$("#prikaziCrte").is(':checked')){
            mElement.strehaCrteSpredaj.visible = false;
            mElement.strehaCrteZadaj.visible = false;
        }
    }
    SCENA.add(streha);
    mElement.streha = streha;

    // mere prikazem samo v primeru, če je odkljukano
    if ($("#prikaziMere").is(':checked') && OBJEKT.elementi.length === MODEL.elementi.length) {
        pripraviMereObjekt();
    }
}

/**
 * Metoda ustvari teraso
 * @param x koordinata X
 * @param y koordinata Y
 * @param mElement modelElement na tej koordinati
 */
var ustvariTerasa = function (x, y){

    var mElement = new ModelElement(x,y);
    MODEL.elementi.push(mElement);
    let smer = 1;
    //postavitev stebrov je odvisna od postavitve rastra na katerega se naslanja
    if (OBJEKT.dobiElement(x - 1,y) == null) {
        smer = -1;
    }

    var fasadaMaterial = new THREE.MeshStandardMaterial({color: OBJEKT.getBarvaFasade()});

    var kockaGeometry = new THREE.BoxBufferGeometry(STEBER_DOLZINA, VISINA-4*TERASA_SIRINA, STEBER_SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    var kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA + smer * (SIRINA/2 - STEBER_SIRINA),0-TERASA_SIRINA,- (y * DOLZINA) + (DOLZINA-TERASA_SIRINA)/2 );
    //console.log (x," ",y," ",x * SIRINA, " ", - y * DOLZINA);
    kocka.name = x.toString() + "_" + y.toString();
    dodajDogodkeNaObjekt(kocka);
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    kocka.add( pripraviRoboveGeometrije(kocka.geometry) );

    kockaGeometry = new THREE.BoxBufferGeometry(STEBER_DOLZINA, VISINA-4*TERASA_SIRINA, STEBER_SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA + smer * (SIRINA/2 - STEBER_SIRINA),0 - TERASA_SIRINA,- (y * DOLZINA) - (DOLZINA-TERASA_SIRINA)/2 );
    kocka.name = x.toString() + "_" + y.toString();
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    kocka.add( pripraviRoboveGeometrije(kocka.geometry) );
    dodajDogodkeNaObjekt(kocka);

    kockaGeometry = new THREE.BoxBufferGeometry(DOLZINA,   TERASA_SIRINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,-VISINA/2 + TERASA_SIRINA*0.5,- (y * DOLZINA) );
    kocka.name = x.toString() + "_" + y.toString();
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    kocka.add( pripraviRoboveGeometrije(kocka.geometry));
    dodajDogodkeNaObjekt(kocka);

    kockaGeometry = new THREE.BoxBufferGeometry(DOLZINA,   3*TERASA_SIRINA, SIRINA);
    kockaGeometry.rotateY(Math.PI /2);
    kocka = new THREE.Mesh(kockaGeometry, fasadaMaterial);
    kocka.renderOrder = 1;
    kocka.position.set(x * SIRINA,VISINA/2 - TERASA_SIRINA*1.5,- (y * DOLZINA) );
    kocka.name = x.toString() + "_" + y.toString();
    SCENA.add(kocka);
    mElement.terasa.push(kocka);
    kocka.add( pripraviRoboveGeometrije(kocka.geometry) );
    dodajDogodkeNaObjekt(kocka);


    //dodam streho
    if (OBJEKT.dobiElement(x,y).dobiStreha() === 0){
        var streha = pripraviRavnoStreho(x,y);
    } else{
        var streha = pripraviDvokapnaStreha(x,y);
        mElement.strehaCrteSpredaj = narisiCrteStreha(x,y,1);
        mElement.strehaCrteZadaj = narisiCrteStreha(x,y,2);
        if (!$("#prikaziCrte").is(':checked')){
            mElement.strehaCrteSpredaj.visible = false;
            mElement.strehaCrteZadaj.visible = false;
        }
    }
    SCENA.add(streha);
    mElement.streha = streha;

    // mere prikazem samo v primeru, če je odkljukano
    if ($("#prikaziMere").is(':checked') && OBJEKT.elementi.length === MODEL.elementi.length) {
        pripraviMereObjekt();
    }


}


/*
Metoda nariše mere objekta v 3D prostor
 */
var pripraviMereObjekt = function(){
    //izbrisem stare mere
    odstraniMere();
    //po vseh rastrih prikažem mere
    for (var i = 0; i < OBJEKT.elementi.length; i++){
        prikazMere(OBJEKT.elementi[i].pozicijaX,OBJEKT.elementi[i].pozicijaY);
    }
    if (OBJEKT.getSteviloElementov()>0) {
        prikazSkupneMere();
    }
}

/**
 * Metoda ustvari plosce z merami in jih prikaze
 * @param x  koordinata rastra
 * @param y - koordinata ratsra
 */
var prikazMere = function (x,y){
    var mElement = MODEL.dobiElement(x,y);

    if (jeSkrajnoLevi(x,y)) {
        var mera = mera_raster(x,y,1,"406 cm")
        mElement.meraD = mera[0];
        mElement.puscicaD1 = mera[1];
        mElement.puscicaD2 = mera[2];
    }

    if (meraJeRobni(x,y)) {
        var mera = mera_raster(x,y,2,"122 cm");
        mElement.meraS = mera[0];
        mElement.puscicaS1 = mera[1];
        mElement.puscicaS2 = mera[2];
    }
}

/**
 * Metoda ustvari plosce z merami in jih prikaze
 * @param x  koordinata rastra
 * @param y - koordinata ratsra
 */
var prikazSkupneMere = function (){
    var okvir = dolociObjektOkvir();
    mera = mera_objekt(okvir,2,((Math.abs(okvir.maxX - okvir.minX) + 1) * SIRINA).toString() + " cm");
    MODEL.meraS = mera[0];
    MODEL.puscicaS1 = mera[1];
    MODEL.puscicaS2 = mera[2];
    mera = mera_objekt(okvir,1,((Math.abs(okvir.maxY - okvir.minY) + 1) * DOLZINA).toString() + " cm");
    MODEL.meraD = mera[0];
    MODEL.puscicaD1 = mera[1];
    MODEL.puscicaD2 = mera[2];
}



function prikazOdstraniElement(x,y){
    for (var i = 0; i < MODEL.elementi.length; i++){
        if (MODEL.elementi[i].pozicijaX === x && MODEL.elementi[i].pozicijaY === y){
            SCENA.remove(MODEL.elementi[i].kocka);
            SCENA.remove(MODEL.elementi[i].streha);
            SCENA.remove(MODEL.elementi[i].strehaCrteSpredaj);
            SCENA.remove(MODEL.elementi[i].strehaCrteZadaj);
            for (var k = 0; k < 4; k++){
                for (var l = 0; l < 3; l++) {
                    SCENA.remove(MODEL.elementi[i].stene[k].odprtina[l].ravnina)
                }
            }
            SCENA.remove(MODEL.elementi[i].meraD);
            SCENA.remove(MODEL.elementi[i].meraS);
            SCENA.remove(MODEL.elementi[i].puscicaD1);
            SCENA.remove(MODEL.elementi[i].puscicaD2);
            SCENA.remove(MODEL.elementi[i].puscicaS1);
            SCENA.remove(MODEL.elementi[i].puscicaS2);
            SCENA.remove(MODEL.elementi[i].hCrtaSpodaj);
            SCENA.remove(MODEL.elementi[i].hCrtaZgoraj);
            SCENA.remove(MODEL.elementi[i].tipDesno);
            SCENA.remove(MODEL.elementi[i].tipLevo);

            for (var k = 0; k < MODEL.elementi[i].terasa.length; k++){
                SCENA.remove(MODEL.elementi[i].terasa[k]);
            }
            MODEL.elementi.splice(i,1);


        }
    }
}

/**
 * Metoda pripravi 3D geometrijo dvokapne strehe.
 * @returns {Geometry}
 */
var geometrijaDvokapnica = function (){
    var geometry = new THREE.Geometry();
    geometry.vertices.push(
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(DOLZINA/2, VISINA/2, 0),
        new THREE.Vector3(DOLZINA, 0, 0),
        new THREE.Vector3(0, 0, SIRINA),
        new THREE.Vector3(DOLZINA/2, VISINA/2, SIRINA),
        new THREE.Vector3(DOLZINA, 0, SIRINA)
    );

    geometry.faces.push(
        new THREE.Face3(0, 1, 2),
        new THREE.Face3(0, 3, 1),
        new THREE.Face3(3, 4, 1),
        new THREE.Face3(4, 5, 1),
        new THREE.Face3(5, 2, 1),
        new THREE.Face3(3, 4, 5)
    );

    geometry.rotateY(Math.PI / 2);
    geometry.computeFaceNormals();
    return geometry;
};


/**
 * Metoda pripravi 3D objekt za dvokapno streho
 * @param x
 * @param y
 * @returns {Mesh}
 */
var pripraviDvokapnaStreha = function (x, y) {
    var geometrija = geometrijaDvokapnica();
    var material = new THREE.MeshStandardMaterial({
        side: THREE.DoubleSide,
        color: OBJEKT.getBarvaStreha()
    });
    var mesh = new THREE.Mesh(geometrija, material);
    mesh.position.set((x * SIRINA) - SIRINA / 2,VISINA/2, - (y * DOLZINA) + DOLZINA / 2);
    mesh.name = x.toString() + "_" + y.toString();
    mesh.cursor = 'pointer';
    dodajDogodkeNaObjekt(mesh);
    mesh.add(pripraviRoboveGeometrije(geometrija));


    return mesh;
};

/**
 * Metoda pripravi mrežo za ge
 * @param geometry
 * @returns {LineSegments}
 */
var pripraviRoboveGeometrije = function (geometry) {
    var geo = new THREE.EdgesGeometry( geometry );
    var mat = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 1 } );
    var wf = new THREE.LineSegments( geo, mat );
    wf.renderOrder = 2;
    return wf;
};

/**
 * MEtoda priprava interaktivno ploščo za ravno streho
 * @param x
 * @param y
 * @returns {Mesh}
 */
var pripraviRavnoStreho = function(x,y){

    var fasadaMaterial = new THREE.MeshBasicMaterial({color: 0xff0000,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0});
    var rasterGeometrija = new THREE.PlaneGeometry(DOLZINA, SIRINA);
    rasterGeometrija.rotateX(Math.PI / 2);
    rasterGeometrija.rotateY(Math.PI /2);
    rasterGeometrija.rotateZ(Math.PI);
    var rasterMesh = new THREE.Mesh(rasterGeometrija, fasadaMaterial);
    rasterMesh.position.set(x * SIRINA,VISINA/2+0.5,- y * DOLZINA);
    rasterMesh.name = x.toString() + "_" + y.toString();
    var geometrija = new THREE.EdgesGeometry( rasterMesh.geometry );
    var crtaMaterial = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 1 } );
    var wireframe = new THREE.LineSegments( geometrija, crtaMaterial );
    wireframe.renderOrder = 2; // make sure wireframes are rendered 2nd
    rasterMesh.add( wireframe );
    dodajDogodkeNaObjekt(rasterMesh);

    return rasterMesh;
}


posodobiOznakeRastrov = function(x,y){
    for (var i = 0; i < OBJEKT.elementi.length; i++) {  //po rastrih
        if (OBJEKT.elementi[i].pozicijaY === y){
            model = MODEL.dobiElement(OBJEKT.elementi[i].pozicijaX,y);
            SCENA.remove(model.tipLevo);
            SCENA.remove(model.tipDesno);
            dodajStranskiTip(OBJEKT.elementi[i].pozicijaX,y,0,model);
            dodajStranskiTip(OBJEKT.elementi[i].pozicijaX,y,1,model);
        }
    }
}




/**
 *  Metoda iz objekta ustvari 3D model in ga postavi na sceno
 */
var ustvariModelObjekta = function (){
    for (var i = 0; i < OBJEKT.elementi.length; i++) {  //po rastrih
        if (OBJEKT.elementi[i].oblika === 3){
            ustvariTerasa(OBJEKT.elementi[i].pozicijaX, OBJEKT.elementi[i].pozicijaY);
        } else if (OBJEKT.elementi[i].oblika === 2){
            ustvariBalkon(OBJEKT.elementi[i].pozicijaX, OBJEKT.elementi[i].pozicijaY);
        } else if (OBJEKT.elementi[i].oblika == 1){
            prikazDodajElement(OBJEKT.elementi[i].pozicijaX, OBJEKT.elementi[i].pozicijaY);
        }
    }
    //prikazemo se oznake rastrov
    var okvir = dolociObjektOkvir();
    for (var y = okvir.minY; y <= okvir.maxY; y++){
        posodobiOznakeRastrov(okvir.minX, y);
    }
}

var dolociDolzinoOdprtine = function (podatekID) {
    if ($.inArray(podatekID,ENOJNA) !== -1)
        return 1;
    if (Math.floor((podatekID - 1) / 3) == 1)
        return 2;
    return 3
}

/**
 *  smer: 1.. dolzina, 2..sirina
 */
var mera_raster = function(x, y, smer, besedilo){

    //puscica
    var dir = new THREE.Vector3( 1, 0, 0 );
    if (smer === 1){
        dir = new THREE.Vector3( 0, 0, 1 );
    }
    dir.normalize();

    var origin = new THREE.Vector3( x * SIRINA, -VISINA/2+1 , -(y + 1) * DOLZINA + DOLZINA/2 -30);
    var length = SIRINA/2;

    if (smer === 1){
        origin = new THREE.Vector3( (x + 1) * SIRINA - SIRINA/2 + 30, -VISINA/2+1 , -(y) * DOLZINA );
        length = DOLZINA/2;
    }
    var hex = 0x000000;
    var ah1 = new THREE.ArrowHelper( dir, origin, length, hex, 10 );
    var ah2 = new THREE.ArrowHelper( dir.negate(), origin, length, hex, 10 );
    SCENA.add( ah1,ah2 );

    //besedilo
    var canvas1 = document.createElement('canvas')
    canvas1.setAttribute('width', 100);
    canvas1.setAttribute('height', 50);
    var context1 = canvas1.getContext('2d');
    context1.font = "Italic 16px Arial";
    context1.fillStyle = "rgba(0,0,0,0.95)";
    context1.fillText(besedilo, 0, 50);
    var texture1 = new THREE.Texture(canvas1);
    texture1.needsUpdate = true;
    var material1 = new THREE.MeshBasicMaterial( {map: texture1, side:THREE.DoubleSide, transparent: true } );
    var mesh1 = new THREE.Mesh(
        new THREE.PlaneGeometry(canvas1.width, canvas1.height),
        material1
    );
    if (smer === 1) {
        mesh1.position.set((x +1) * SIRINA - SIRINA / 3 + 10, -VISINA / 2 + 1.02, -y * DOLZINA + 7);
        mesh1.rotateX(-Math.PI / 2);
        mesh1.rotateZ(Math.PI / 2);
    }
    else {
        mesh1.position.set(x * SIRINA - SIRINA/5,- VISINA/2 + 1.01,-(y+1)* DOLZINA + DOLZINA/3 + 40);
        mesh1.rotateX(-Math.PI / 2);
        mesh1.rotateZ(Math.PI);
    }

    SCENA.add( mesh1 );
    return [mesh1, ah1, ah2];
}

/**
 *  smer: 1.. dolzina, 2..sirina
 */
var mera_objekt = function( okvir, smer, besedilo){

    //puscica
    var dir = new THREE.Vector3( 1, 0, 0 );
    if (smer === 1){
        dir = new THREE.Vector3( 0, 0, 1 );
    }
    dir.normalize();

    var razlikaX = Math.abs(okvir.maxX - okvir.minX);
    var razlikaY = Math.abs(okvir.maxY - okvir.minY);


    var origin = new THREE.Vector3( (okvir.maxX - razlikaX / 2) * SIRINA, -VISINA/2+1 , -(okvir.maxY + 1) * DOLZINA + DOLZINA/3);
    var length = (razlikaX+1)/2 * SIRINA;

    if (smer === 1){
        origin = new THREE.Vector3( (okvir.maxX + 1) * SIRINA, -VISINA/2+1 , -(okvir.maxY - razlikaY/2) * DOLZINA );
        length = (razlikaY+1)/2 * DOLZINA;
    }
    var hex = 0x000000;
    var ah1 = new THREE.ArrowHelper( dir, origin, length, hex, 10 );
    var ah2 = new THREE.ArrowHelper( dir.negate(), origin, length, hex, 10 );
    SCENA.add( ah1,ah2 );

    //besedilo
    var canvas1 = document.createElement('canvas')
    canvas1.setAttribute('width', 100);
    canvas1.setAttribute('height', 50);
    var context1 = canvas1.getContext('2d');
    context1.font = "Italic 16px Arial";
    context1.fillStyle = "rgba(0,0,0,0.95)";
    context1.fillText(besedilo, 0, 50);
    var texture1 = new THREE.Texture(canvas1);
    texture1.needsUpdate = true;
    var material1 = new THREE.MeshBasicMaterial( {map: texture1, side:THREE.DoubleSide, transparent: true } );
    var mesh1 = new THREE.Mesh(
        new THREE.PlaneGeometry(canvas1.width, canvas1.height),
        material1
    );
    if (smer === 1) {
        mesh1.position.set((okvir.maxX +1) * SIRINA, -VISINA / 2 + 2, -(okvir.maxY - razlikaY/2) * DOLZINA + 7);
        mesh1.rotateX(-Math.PI / 2);
        mesh1.rotateZ(Math.PI / 2);
    }
    else {
        mesh1.position.set((okvir.maxX - razlikaX / 2) * SIRINA - SIRINA/5,- VISINA/2 + 2,-(okvir.maxY+1)* DOLZINA + DOLZINA/3);
        mesh1.rotateX(-Math.PI / 2);
        mesh1.rotateZ(Math.PI);
    }

    SCENA.add( mesh1 );

    return [mesh1, ah1, ah2];
}


/**
 * Narise horizontalno črto in jo vrne
 *
 * @param x koordinata
 * @param y koordinata
 * @param tip spodnja ali zgornja črta
 */
var narisiHorizontalnoCrto = function (x,y, tip){
    var material = new THREE.LineBasicMaterial( { color: 0xffffff } );
    var points = [];
    var visina  = - VISINA/2 + TERASA_SIRINA;
    if (tip === 2) {
        visina  = VISINA/2 - 3 * TERASA_SIRINA;
    }


    //zadaj dolga
    points.push( new THREE.Vector3( x * SIRINA - SIRINA/2 - 1, visina, -(y) * DOLZINA + DOLZINA/2+1 ) );
    points.push( new THREE.Vector3( x * SIRINA - SIRINA/2 - 1, visina, -(y) * DOLZINA - DOLZINA/2 - 1 ) );
    points.push( new THREE.Vector3( x * SIRINA + SIRINA/2 +1, visina, -(y) * DOLZINA - DOLZINA/2  - 1 ) );
    points.push( new THREE.Vector3( x * SIRINA + SIRINA/2 +1, visina , -(y) * DOLZINA + DOLZINA/2 +1 ) );
    points.push( new THREE.Vector3( x * SIRINA - SIRINA/2 - 1, visina, -(y) * DOLZINA + DOLZINA/2+1 ) );

    var geometry = new THREE.BufferGeometry().setFromPoints( points );
    var line = new THREE.Line( geometry, material );
    SCENA.add(line);

    return line;
}

/**
 * metoda nariše črte na dvokapnico
 * @param x
 * @param y
 * @param tip - 1 sprednja, zadnja stran
 * @returns {Line<BufferGeometry, LineBasicMaterial>}
 */
var narisiCrteStreha = function (x,y, tip) {
    var sirina = x * SIRINA + SIRINA / 2 + 0.5;  //sprednja stran
    if (tip === 2) {
        sirina = x * SIRINA - SIRINA / 2 - 0.5;  //zadnja stran
    }

    var dolzina = -(y) * DOLZINA; //sredina kvadra
    var visina = VISINA/2;
    var material = new THREE.LineBasicMaterial( { color: 0xffffff } );
    var points = [];
    points.push( new THREE.Vector3( sirina , visina, dolzina + DOLZINA/2 - TERASA_SIRINA)  ); //tocka 1
    points.push( new THREE.Vector3( sirina , visina + 15, dolzina + DOLZINA/2 - TERASA_SIRINA)  ); //tocka 2
    points.push( new THREE.Vector3( sirina , visina, dolzina + DOLZINA/2 - TERASA_SIRINA)  ); //tocka 3
    points.push( new THREE.Vector3( sirina , visina, dolzina + DOLZINA/2 - TERASA_SIRINA - SIRINA) ); //tocka 4
    points.push( new THREE.Vector3( sirina , visina + 112, dolzina + DOLZINA/2 - TERASA_SIRINA - SIRINA) ); //tocka 5
    points.push( new THREE.Vector3( sirina , visina, dolzina + DOLZINA/2 - TERASA_SIRINA - SIRINA) ); //tocka 6
    points.push( new THREE.Vector3( sirina , visina, dolzina - DOLZINA/2 + TERASA_SIRINA + SIRINA) ); //tocka 7
    points.push( new THREE.Vector3( sirina , visina + 112, dolzina - DOLZINA/2 + TERASA_SIRINA + SIRINA) );//tocka 8
    points.push( new THREE.Vector3( sirina , visina, dolzina - DOLZINA/2 + TERASA_SIRINA + SIRINA) );//tocka 9
    points.push( new THREE.Vector3( sirina , visina, dolzina - DOLZINA/2 + TERASA_SIRINA) );//tocka 10
    points.push( new THREE.Vector3( sirina , visina + 15, dolzina - DOLZINA/2 + TERASA_SIRINA) );//tocka 11

    var geometry = new THREE.BufferGeometry().setFromPoints( points );
    var line = new THREE.Line( geometry, material );
    SCENA.add(line);

    return line;
}

/**
 * metoda nariše robne črte na teraso, ker jih na kockah ne moremo uporabiti.
 * @param x
 * @param y
 * @param tip - 1 zunanja in notranja
 * @returns {Line<BufferGeometry, LineBasicMaterial>}
 */
var narisiCrteTerasaObod = function (x,y,tip) {
    var ssirina = x * SIRINA + SIRINA / 2;  //sprednja stran
    var zsirina = x * SIRINA - SIRINA / 2;  //zadnja stran
    var ldolzina = -(y) * DOLZINA + DOLZINA/2;
    var ddolzina = -(y) * DOLZINA - DOLZINA/2;
    var svisina = -VISINA/2;
    var zvisina = VISINA/2;
    if (tip === 2){
        ldolzina = -(y) * DOLZINA + DOLZINA/2 - TERASA_SIRINA-0.5;
        ddolzina = -(y) * DOLZINA - DOLZINA/2 + TERASA_SIRINA + 0.5;
        svisina  = - VISINA/2 + TERASA_SIRINA + 0.1;
        zvisina  = VISINA/2 - 3 * TERASA_SIRINA - 0.1;
    }

    var material = new THREE.LineBasicMaterial( { color: 0xffffff } );
    var points = [];
    points.push( new THREE.Vector3( ssirina , svisina, ldolzina)  ); //tocka 1
    points.push( new THREE.Vector3( ssirina , svisina, ddolzina)  ); //tocka 1
    points.push( new THREE.Vector3( zsirina , svisina, ddolzina)  ); //tocka 1
    points.push( new THREE.Vector3( zsirina , svisina, ldolzina)  ); //tocka 1
    points.push( new THREE.Vector3( ssirina , svisina, ldolzina)  ); //tocka 1
    points.push( new THREE.Vector3( ssirina , zvisina, ldolzina)  ); //tocka 6
    points.push( new THREE.Vector3( ssirina , zvisina, ddolzina)  ); //tocka 7
    points.push( new THREE.Vector3( ssirina , svisina, ddolzina)  ); //tocka 8
    points.push( new THREE.Vector3( ssirina , zvisina, ddolzina)  ); //tocka 9
    points.push( new THREE.Vector3( zsirina , zvisina, ddolzina)  ); //tocka 10
    points.push( new THREE.Vector3( zsirina , svisina, ddolzina)  ); //tocka 11
    points.push( new THREE.Vector3( zsirina , zvisina, ddolzina)  ); //tocka 12
    points.push( new THREE.Vector3( zsirina , zvisina, ldolzina)  ); //tocka 12
    points.push( new THREE.Vector3( ssirina , zvisina, ldolzina)  ); //tocka 6
    points.push( new THREE.Vector3( zsirina , zvisina, ldolzina)  ); //tocka 12
    points.push( new THREE.Vector3( zsirina , svisina, ldolzina)  ); //tocka 1

    var geometry = new THREE.BufferGeometry().setFromPoints( points );
    var line = new THREE.Line( geometry, material );
    SCENA.add(line);

    return line;
}

/**
 * metoda nariše robne črte na teraso, ker jih na kockah ne moremo uporabiti.
 * @param x
 * @param y
 * @param tip - 1 zunanja in notranja
 * @returns {Line<BufferGeometry, LineBasicMaterial>}
 */
var narisiCrteTerasaVmesne = function (x,y,tip) {
    var VISINA_CRTE = 3 * TERASA_SIRINA;

    var sirina = x * SIRINA + SIRINA / 2 + 0.2;  //sprednja stran
    if (tip > 2) {
        sirina = x * SIRINA - SIRINA / 2 - 0.2;  //zadnja stran
    }
    var dolzina = -(y) * DOLZINA; //sredina kvadra

    var visina = VISINA/2 ;
    if (tip === 2 || tip === 4){
        visina = - VISINA/2 + TERASA_SIRINA;
        VISINA_CRTE = TERASA_SIRINA;
    }




    var material = new THREE.LineBasicMaterial( { color: 0xffffff } );
    var points = [];
    points.push( new THREE.Vector3( sirina , visina, dolzina + DOLZINA/2 - TERASA_SIRINA)  ); //tocka 1
    points.push( new THREE.Vector3( sirina , visina -  VISINA_CRTE, dolzina + DOLZINA/2 - TERASA_SIRINA)  ); //tocka 2
     points.push( new THREE.Vector3( sirina , visina - VISINA_CRTE, dolzina + DOLZINA/2 - TERASA_SIRINA - SIRINA)  ); //tocka 3
     points.push( new THREE.Vector3( sirina , visina, dolzina + DOLZINA/2 - TERASA_SIRINA - SIRINA) ); //tocka 4
    points.push( new THREE.Vector3( sirina , visina - VISINA_CRTE, dolzina + DOLZINA/2 - TERASA_SIRINA - SIRINA)  ); //tocka 5
    points.push( new THREE.Vector3( sirina , visina - VISINA_CRTE,dolzina - DOLZINA/2 + TERASA_SIRINA + SIRINA) );
    points.push( new THREE.Vector3( sirina , visina,dolzina - DOLZINA/2 + TERASA_SIRINA + SIRINA) );
    points.push( new THREE.Vector3( sirina , visina - VISINA_CRTE,dolzina - DOLZINA/2 + TERASA_SIRINA + SIRINA) );//tocka 8
    points.push( new THREE.Vector3( sirina , visina - VISINA_CRTE,dolzina - DOLZINA/2 + TERASA_SIRINA) );
    points.push( new THREE.Vector3( sirina , visina,dolzina - DOLZINA/2 + TERASA_SIRINA) );


    var geometry = new THREE.BufferGeometry().setFromPoints( points );
    var line = new THREE.Line( geometry, material );
    line.name = "crta";
    SCENA.add(line);

    return line;
}

