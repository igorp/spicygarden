### Objektni model za Pick&Build objekte

####Objekt :: class
Krovni objekt.

*atributi:*
+ version  -- potrebno, če se bo model nadgrajeval
+ barvaFasada -- trenutno fiksirano
+ barvaStreha -- trenutno fiksirano
+ elementi = [ ] -- seznam objektov razreda _Element_ rastrov (osnovnih elementov)

####Element :: class
Posamezen raster(element), ki ga sestavlja 6 sten. 

Stene si sledijo (gledano od spredaj): desno (0), levo (1), spredaj (2), zadaj (3), zgoraj (4), spodaj (5).
Stene 4 in 5 se trenutno ne spreminjajo.

*atributi:*
+ pozicijaX -- X koordinata v mrežnem tlorisu
+ pozicijaY -- Y koordinata v mrežnem tlorisu
+ tip -- tip objekta (zaključni, vmesni, ..)
+ stene = [ ] -- seznam objektov razreda _Stena_. 
+ streha -- oblika stene: 0 - ravna, 1 - dvokapna
+ zakljucni -- ali je element zaključni (na čelni strani)
+ robni --  ali je element zaključni (na kapni strani)
+ terasa -- true, če je element prikazan kot terasa


####Stena :: class
Posamezna stena, ki se vsebuje podatke o poziciji na elementu/rastru in odprtinah.

*atributi:*
+ pozicija -- pozicija stene na elementu. Glej opis elementa.
+ vidna -- ali je stena zunanja ali notranja
+ odprtina = [ ] -- seznam odprtin. Na kapni je lahko ena, na čelni so lahko tri.

####Odprtina :: class
Odprtina na steni.

*atributi:*
+ obstaja -- ali odprtina kaj vsebuje;
+ id -- id vsebuje podatek, kakšno obliko odprtine vsebuje
+ pozicija -- pozicija odprtine na steni: levo (0), sredina (1), desno (2)
+ zacetna -- podatek pove ali se odprtina začne na tej poziciji. Pomembno pri sestavljenih odprtinah
+ povezava = [ ] -- seznam povezanih odprtin. Pomembno pri brisanju odprtin