


/**
 * toggle cell representing element in house
 * @param col  column
 * @param row  row
 */
function toggle_cell(row, col, elt){
    hisa.toggleCell(row, col);
    if (hisa.getCellStatus(row,col) == 1){
        $(elt).addClass("spicy_element");
        $(elt).addClass("context-menu-one");
    }
    else {
        $(elt).removeClass("spicy_element");
        $(elt).removeClass("context-menu-one");
    }
}

/**
 * restore to initial state
 *
 */
function reset(){
    hisa.resetHouse();
    for (i = 0; i < hisa.m; i++){
        for (j = 0; j < hisa.n; j++){
            $("#"+i+"_"+j).removeClass("spicy_element");
            $("#"+i+"_"+j).removeClass("context-menu-one");
        }
    }
    $("#info").html("");
}

/**
 * Method checks if are allowed tu put another raster
 * @param row
 * @param col
 * @returns {boolean}
 */
function is_allowed(row, col){
    //za prvi kvadratek dovolimo kjerkoli
    if (hisa.prestejStAktivnihElementov()<1)
        return true;

    var row = parseInt(row);
    var col = parseInt(col);
    //locimo ali dodajamo ali brisemo.
    //dodajanje
    if (hisa.getCellStatus(row,col) == 0) {

        if (row + 1 < m && hisa.getCellStatus(row + 1, col) == 1) {
            return true;
        }
        if (row - 1 >= 0 && hisa.getCellStatus(row - 1, col) == 1) {
            return true;
        }
        if (col + 1 < n && hisa.getCellStatus(row, col + 1) == 1) {
            return true;
        }
        if (col - 1 >= 0 && hisa.getCellStatus(row, col - 1) == 1) {
            return true;
        }
        return 2;
    }
    else { //brisanje
        var stSosedov = 0;
        if (row + 1 < m && hisa.getCellStatus(row + 1, col) == 1) {
            stSosedov = stSosedov + 1;
        }
        if (row - 1 >= 0 && hisa.getCellStatus(row - 1, col) == 1) {
            stSosedov = stSosedov + 1;
        }
        if (col + 1 < n && hisa.getCellStatus(row, col + 1) == 1) {
            stSosedov = stSosedov + 1;
        }
        if (col - 1 >= 0 && hisa.getCellStatus(row, col - 1) == 1) {
            stSosedov = stSosedov + 1;
        }

        if (stSosedov > 1)
            return 3;
        return true;
    }

}


/**
 * Dogodki
 */

/**
 * funkcija preveri checkbutton za prikaz mreže rastrov
 */
var chkbChangeMreza  = function (){
    if($("#chkb_mreza").is(':checked'))
        toogleMreza(hisa,true);
    else
        toogleMreza(hisa,false);
};


var clickDolociBarvoStrehe = function(){
    picker1.show();
};

var clickDolociBarvoFasade = function(){
    picker2.show();
};


/**
 *  ThreeJS funkcije
 */

var geometrijaDvokapnica = function (){
    var geometry = new THREE.Geometry();
    geometry.vertices.push(

        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(200, 150, 0),
        new THREE.Vector3(400, 0, 0),
        new THREE.Vector3(0, 0, 122),
        new THREE.Vector3(200, 150, 122),
        new THREE.Vector3(400, 0, 122));

    geometry.faces.push(

        new THREE.Face3(0, 1, 2),
        new THREE.Face3(0, 3, 1),
        new THREE.Face3(3, 4, 1),
        new THREE.Face3(4, 5, 1),
        new THREE.Face3(5, 2, 1),
        new THREE.Face3(3, 4, 5));


    geometry.computeFaceNormals();
    return geometry;
};



var generirajStreha = function (geometrija, t, v) {

    var material;
    material = new THREE.MeshStandardMaterial({
        side: THREE.DoubleSide,
        color: hisa.getBarvaStreha()
    });
    var mesh = new THREE.Mesh(geometrija, material);
    mesh.position.set(t-200,160,v-60);

    return mesh;

};

var generirajStrehaMreza = function (geometry) {
    var geo = new THREE.EdgesGeometry( geometry );
    var mat = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 1 } );
    WIREFRAME = new THREE.LineSegments( geo, mat );
    wireframe.renderOrder = 2;

    return wireframe;
};


/**
 * Function changes color of the house, based on the user input using dat.gui component
 * @param hisa object
 * @param color user defined color
 */
function spremeniFasadaBarva(hisa, color){
    hisa.setBarvaFasade(color);
    var barva = new THREE.Color(color);
    for(var i = 0; i<hisa.elementi.length; i++){
        hisa.elementi[i].material.color.setHex(barva.getHex());
    }
}

/**
 * Function changes color of the house, based on the user input using dat.gui component
 * @param hisa object
 * @param color user defined color
 */
function spremeniStrehaBarva(hisa, color){
    hisa.setBarvaStrehe(color);
    var barva = new THREE.Color(color);
    for(var i = 0; i<hisa.ostresje.length; i++){
        hisa.ostresje[i].material.color.setHex(barva.getHex());
    }
}



/**
 *  Pomozne funkcije za izgled
 */

/**
 * Funkcija prikaze ali skrije mrezo rastrov.
 *
 * @param hisa object
 * @param value value for the toogle - from the dat.gui component
 */
function toogleMreza(hisa, value) {
    for(var i = 0; i<hisa.elementi.length; i++){
        hisa.elementi[i].children[0].visible = value;
    }
    for(var i = 0; i<hisa.ostresje.length; i++){
        hisa.ostresje[i].children[0].visible = value;
    }
}

var rbClickTipStrehe = function (elt) {
    for (i = 0; i < hisa.m; i++) {
        for (j = 0; j < hisa.n; j++) {
            if (hisa.getCellStatus(i, j)) {
                var streha = hisa.getElement(i, j).streha;
                streha.setVrsta(elt.value);
            }
        }
    }
    hisa.uniciObjekte(); //TODO izbrisati samo streho, če ni bila ravna
    hisa.updateVidneStene();
    //ustvarimo hiso
    ustvari_hiso(hisa,SCENA);
    renderer.render( SCENA, camera );
}


var rbClickPosodobiHiso = function (elt) {
    hisa.uniciObjekte();
    var center_hisa = ustvari_hiso(hisa,SCENA);
    renderer.render( SCENA, camera );
}



var dodatekClick = function (elt) {
    if ($(elt).hasClass("imgFocus")){
        $(elt).removeClass("imgFocus");
        return;
    }
    // vsem najprej izbrisemo ta razred.
    $(".imgFocus").each(function () {
        $(this).removeClass("imgFocus");
    });

    $(elt).addClass("imgFocus");
};

