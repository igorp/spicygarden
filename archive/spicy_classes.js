
/**
 * Objekt za stene
 * vidna .. je stena vidna v osnovnem prikazu
 * okno .. ima okno
 * okno_img .. datoteka za okno
 * vrata .. ima vrata
 * vrata_img .. datoteka za prikaz vrat
 * notranja .. je notranja vmesna stena, vidna mora biti v tem primeru false
 *
 */
class Stena{
    constructor(pozicija) {
        this.pozicija = pozicija;
        this.id = 1;
        this.vidna = true;
        this.okno = [false, false, false];
        this.okno_img = ["","",""];
        this.vrata = [false, false, false];
        this.vrata_img = ["","",""];
        this.povezane = [[],[],[]]; // pri vecjih sirinah oken, vsak vertikala lahko ima povezane
        this.notranja = 0;
    }

    setOkno(filename,pos = 0){
        this.okno[pos] = true;
        this.okno_img[pos] = filename;
    }

    getOkno(pos = 0){
        return this.okno_img[pos];
    }

    imaOkno(pos = 0){
        return this.okno[pos] == 1;
    }

    odstraniOkno(pos = 0){
        this.okno[pos] = false;
        this.okno_img[pos] = "";
    }

    setVrata(filename, pos = 0){
        this.vrata[pos] = true;
        this.vrata_img[pos] = filename;
    }

    getVrata(pos = 0){
        return this.vrata_img[pos];
    }

    imaVrata(pos = 0){
        return this.vrata[pos] == true;
    }

    odstraniVrata(pos = 0){
        this.vrata[pos] = false;
        this.vrata_img[pos] = "";
    }


    setNotranja(value){
        this.notranja = value;
        if (this.notranja == true){
            this.vidna = false;
        }
    }

    getNotranja(){
       return this.notranja;
    }

    jeNotranja(){
        return this.notranja == 0;
    }

    setVidna(value){
        this.vidna = value;
        if (this.vidna == true){
            this.notranja = false;
        }
    }

    getVidna(){
        return this.vidna;
    }

    jeVidna(){
        return this.vidna == true;
    }

    jeKapna(){
        if (this.pozicija == 0 || this.pozicija == 1)
            return true;
        return false;
    }

    jeCelna(){
        if (this.pozicija == 2 || this.pozicija == 3)
            return true;
        return false;
    }
}

/**
 * @param tip .. tip strehe. ima tri moznosti
 *  0 .. ravna
 *  1 .. enokapnica
 *  2 .. dvokapnica
 *  @param barva .. v hex rgb sistemu
 */
class Streha{
    constructor(tip = 0){
        this.vrsta = tip;
    }

    getVrsta(){
        return this.vrsta;
    }

    setVrsta(tip){
        this.vrsta = tip;
    }
}

/**
 * Razred za osnovne elemente (rastre) hiše.
 *
 * seznam sten [ 0 desno,1 levo, 2 spredaj, 3 zadaj, 4 zgoraj, 5 spodaj]
 */
class Element{
    constructor(active = 0){
        this.active = active;
        this.stene = [new Stena(0),new Stena(1),new Stena(2),new Stena(3),new Stena(4),new Stena(5)]
        this.streha = new Streha();
        this.zakljucni = false;
    }


}

class Hisa{
    constructor(){
        this.id = 1;
        this.barva_fasada = '#271e1f';
        this.barva_streha = '#271e1f';
        this.matrix = [];
        this.elementi = [];
        this.ostresje = [];
        this.obod = [];
        this.startI = null;  //uporabljam pri izračunu pozicije v ravnini.
        this.startJ = null;
        this.m = 10;
        this.n = 6;
        var i,j;

        for (i = 0; i <= this.m; i++){
            this.matrix[i] = [];
            for (j = 0; j <= this.n; j++){
                this.matrix[i][j] = new Element(0);
            }
        }
    };

    getElement(i,j){
        return this.matrix[i][j];

    }

    getBarvaFasade(){
        return this.barva_fasada;
    }

    setBarvaFasade(barva){
        this.barva_fasada = barva;
    }

    getBarvaStreha(){
        return this.barva_streha;
    }

    setBarvaStrehe(barva){
        this.barva_streha = barva;
    }

    /**
     * toogles state of the cell
     * @param row
     * @param col
     */
    toggleCell(row, col){
        if (this.matrix[row][col].active == 0){
            if (this.prestejStAktivnihElementov()<2){
                this.matrix[row][col].zakljucni = true;
            }
            this.matrix[row][col].active = 1;
        }
        else {
            this.matrix[row][col].active = 0
        }
    }

    /**
     * sets state of the cell to state
     * @param row
     * @param col
     * @param state
     */
    setCell(row, col, state){
        this.matrix[row][col].active = state;
    }

    getCellStatus(row,col){
        return this.matrix[row][col].active;
    }

    resetHouse(){
        var i,j;
        this.uniciObjekte();
        for (i = 0; i <= this.m; i++){
            for (j = 0; j <= this.n; j++){
                this.matrix[i][j] = new Element(0);
            }
        }
    }

    /**
     * funkcija unici vse ThreeJS objekte
     */
    uniciObjekte(){
        //elementi
        while (this.elementi.length>0){
            var e = this.elementi.shift();
            SCENA.remove(e);
            //e.dispose();
        }

        //streha
        while (this.ostresje.length>0){
            var e = this.ostresje.shift();
            SCENA.remove(e);
            //e.dispose();
        }

        while (this.obod.length>0){
            var e = this.obod.shift();
            SCENA.remove(e);
            //e.dispose();
        }
    }


    prestejStAktivnihElementov() {
        var k = 0;
        for (var i = 0; i < this.m; i++) {
            for (var j = 0; j < this.n; j++) {
                if (this.getCellStatus(i,j) == 1){
                    k = k + 1;
                }
            }
        }
        return k;
    }

    updateVidneStene(){
        var i,j;
        var Nactives = this.prestejStAktivnihElementov();
        for (i = 0; i < this.m; i++) {
            for (j = 0; j < this.n; j++) {
                if (this.matrix[i][j].active){
                    var zadaj = false;
                    var spredaj = false;

                    if (i > 0)  //zadaj
                        if (this.matrix[i-1][j].active){
                            this.matrix[i][j].stene[3].setVidna(false);
                            zadaj = true;
                        }

                    if (i < this.m) //spredaj
                        if (this.matrix[i+1][j].active){
                            this.matrix[i][j].stene[2].setVidna(false);
                            spredaj = true;
                        }
                    if (j > 0)  //levo
                        if (this.matrix[i][j-1].active){
                            this.matrix[i][j].stene[1].setVidna(false);
                    }
                    if (j < this.n) //desno
                        if (this.matrix[i][j+1].active){
                            this.matrix[i][j].stene[0].setVidna(false);
                    }
                    if (Nactives > 1) {
                        this.matrix[i][j].zakljucni = true;
                        if (spredaj == zadaj) {
                            this.matrix[i][j].zakljucni = false;
                        }
                    }

                }
            }
        }
    }

}
