/*
Author: Igor Pesek
Copyright: 2019,2020
 */

/**
 * Funkcija zamenja za izbrano steno (ki omogoča klik) prikazno steno.
 * @param name  ime stene
 * @param material material, ki ga bomo zamenjali
 */
var zamenjajMaterialStene = function (name, material) {
    for (var st = 0; st < hisa.obod.length ; st++) { //poiscem pravo steno
        if (hisa.obod[st].name == name){
            hisa.obod[st].material = material;
        }
    }
};

/**
 * izbrise steno in povezane stene
 * @param stena
 * @param pozicijaOkna
 */
var izbrisiDodatkeStene = function (ime, stena, pozicijaOkna) {
    stena.odstraniOkno(pozicijaOkna);  //izbrisem sebe in ponastavim fasado
    var ravninaMat = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide,transparent:true, opacity:0} );
    ime = ime.slice(0, -1) + pozicijaOkna;
    zamenjajMaterialStene(ime,ravninaMat);
    while (stena.povezane[pozicijaOkna].length>0){
        var povezana =  stena.povezane[pozicijaOkna].pop();
        stena.odstraniOkno(povezana);
        ime = ime.slice(0, -1) + povezana;
        zamenjajMaterialStene(ime,ravninaMat);
        stena.povezane[povezana] = [];
    }
    test = true;
};


var pripraviSteno= function (ev){
    var pozicija = $.map(ev.data.target.name.split('_'), function(value){
        return parseInt(value, 10);
    });
    var element = hisa.getElement(pozicija[0],pozicija[1]);

    var curStena = element.stene[pozicija[2]];

    var ravninaMat;

    // ko je stena kliknjena
    if ($(".imgFocus").length == 1) {//nekaj je izbrano med dodatki

        if ($(".imgFocus").first().hasClass("kos")){ //brisemo vse
            izbrisiDodatkeStene(ev.data.target.name,curStena,pozicija[3]);
        }
        else if (element.zakljucni && curStena.jeKapna() ){
            alert("V robne rastre ni mogoče dodajati odprtin.");
            return;
        }
        else if ($(".imgFocus").first().hasClass("okno") && ($(".imgFocus").first().hasClass("len2") || $(".imgFocus").first().hasClass("len3")) && curStena.jeKapna()) {
            alert("V robne rastre ni mogoče dodajati širokih odprtin.");
            return;
        }
        else if ($(".imgFocus").first().hasClass("streha")){  //ce spreminjamo streho
            if (curStena.jeCelna() ){  //sprednja ali zadnja stena.
                var value = 2;
                if ($(".imgFocus").first().hasClass("ravna"))
                    value = 0;

                for (i = 0; i < hisa.m; i++) {
                    if (hisa.getCellStatus(i, pozicija[1])) {
                        var streha = hisa.getElement(i, pozicija[1]).streha;
                        streha.setVrsta(value);
                    }
                }
            }
            // izbrisem izbiro
            izbrisiRazred("imgFocus");
            return 1;
        }
        else if ($(".imgFocus").first().hasClass("okno")) { //dodajam okno
            if ($(".imgFocus").first().hasClass("len2")){
                var pozicijaLevo = pozicija[3];
                if (pozicijaLevo == 2) pozicijaLevo = 1;
                // če je bila povezana, potem je potrebno staro izbrisati
                izbrisiDodatkeStene(ev.data.target.name,curStena,pozicijaLevo);
                izbrisiDodatkeStene(ev.data.target.name,curStena,pozicijaLevo+1);

                var extension = ["l","d"];
                for (var i = 0; i < 2; i++) {
                    var src = $(".imgFocus").first().attr("src");
                    src = src.spicy_insert(extension[i]);
                    curStena.setOkno(src, i + pozicijaLevo);
                    var tekstura = LOADER.load(src);
                    ravninaMat = new THREE.MeshBasicMaterial({side: THREE.DoubleSide, map: tekstura,transparent: true, alphaTest: 0.5});
                    var ime_stene = ev.data.target.name.slice(0, -1) + (i+pozicijaLevo).toString();
                    zamenjajMaterialStene(ime_stene, ravninaMat);
                }
                if (pozicijaLevo == 0){
                    curStena.povezane[0].push(1);
                    curStena.povezane[1].push(0);
                }
                else {
                    curStena.povezane[1].push(2);
                    curStena.povezane[2].push(1);
                }
            }
            else if ($(".imgFocus").first().hasClass("len3")){
                var pozicijaLevo = 0;
                //izbrisemo vse tri
                var extension = ["l","s","d"];
                var imgsrc = $(".imgFocus").first().attr("src");
                for (var i = 0; i < 3; i++) {
                    src = imgsrc.spicy_insert(extension[i]);
                    curStena.setOkno(src, i);
                    var tekstura = LOADER.load(src);
                    ravninaMat = new THREE.MeshBasicMaterial({side: THREE.DoubleSide, map: tekstura,transparent: true, alphaTest: 0.5});
                    var ime_stene = ev.data.target.name.slice(0, -1) + i.toString();
                    zamenjajMaterialStene(ime_stene, ravninaMat);
                }
                curStena.povezane[0] = [1,2];
                curStena.povezane[1] = [0,2];
                curStena.povezane[2] = [0,1];
            }
            else {
                var src = $(".imgFocus").first().attr("src");
                //ima stena že okno? Potem ga moramo najprej izbrisati
                if (curStena.imaOkno(pozicija[3]))
                    izbrisiDodatkeStene(ev.data.target.name,curStena,pozicija[3]);

                curStena.setOkno(src, pozicija[3]);
                var tekstura = LOADER.load(src);
                ravninaMat = new THREE.MeshBasicMaterial({
                    side: THREE.DoubleSide,
                    map: tekstura,
                    transparent: true,
                    alphaTest: 0.5
                });
                zamenjajMaterialStene(ev.data.target.name,ravninaMat);
            }
        }

        // izbrisem izbiro
        izbrisiRazred("imgFocus");
    }
    return 0;
};


function ustvari_hiso(hisa, scena){
    var i,j;

    //vsi elementi imajo enako fasado, zato lahko ustvarim enkrat.
    var fasadaMaterial;
    fasadaMaterial = new THREE.MeshStandardMaterial({color: hisa.getBarvaFasade()});

    var t,v; //pozicija elementa v ravnini. y vertikala
    for (i = 0; i < hisa.m; i++) {
        for (j = 0; j < hisa.n; j++) {
            if (hisa.getCellStatus(i, j)) {
                var element = hisa.getElement(i,j);
                // ustvarimo element
                var cubeGeometry = new THREE.BoxBufferGeometry(DOLZINA, VISINA, SIRINA);
                var cube = new THREE.Mesh(cubeGeometry, fasadaMaterial);
                cube.renderOrder = 1;
                // prvi element narišemo vedno v 0, 0, 0, ostale prilagodimo. Tako je objekt vedno centriran
                if (j > hisa.startJ){
                    t = (j - hisa.startJ) * DOLZINA;
                }
                else {
                    t = (hisa.startJ - j) * (-DOLZINA);
                }
                if (i > hisa.startI){
                    v = (i - hisa.startI) * SIRINA;
                }
                else {
                    v = (hisa.startI - i) * (-SIRINA);
                }

                cube.position.set(t,0,v);
                scena.add(cube);

                // dodamo mrezo
                var geo = new THREE.EdgesGeometry( cube.geometry );
                var mat = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 1 } );
                wireframe = new THREE.LineSegments( geo, mat );
                wireframe.renderOrder = 2; // make sure wireframes are rendered 2nd
                cube.add( wireframe );



                //[desno, levo, spredaj, zadaj, zgoraj, spodaj,]
                //pregledam vse stene in izrisem zunanje
                var posX = [t+(DOLZINA/2)+1, t-(DOLZINA/2)-1, t, t];
                var posZ = [v, v, v+SIRINA/2+1, v-SIRINA/2-1];

                for (var k = 0; k < 4; k++){  //tal in stropa zaenkrat ne delam.
                    if (element.stene[k].jeVidna()){
                        if (element.stene[k].jeKapna()) {
                            var ravninaGeo = new THREE.PlaneGeometry(SIRINA, VISINA);
                            ravninaGeo.rotateY(Math.PI / 2);
                            if (element.stene[k].imaOkno()) {
                                var tekstura = LOADER.load(element.stene[k].getOkno());
                                var ravninaMat = new THREE.MeshBasicMaterial({
                                    side: THREE.DoubleSide,
                                    map: tekstura,
                                    transparent: true,
                                    alphaTest: 0.5
                                });
                            } else {
                                var ravninaMat = new THREE.MeshBasicMaterial({
                                    color: 0xffff00,
                                    side: THREE.DoubleSide,
                                    transparent: true,
                                    opacity: 0
                                });
                            }

                            var ravnina = new THREE.Mesh(ravninaGeo, ravninaMat);
                            //nastavim pozicijo
                            ravnina.position.set(posX[k], 0, posZ[k]);
                            ravnina.name = i.toString() + "_" + j.toString() + "_" + k.toString() + "_0";

                            //Interaktivnost
                            ravnina.cursor = 'pointer';

                            ravnina.on('click', function (ev) {
                                if (pripraviSteno(ev) == 1) {
                                    hisa.uniciObjekte(); //TODO izbrisati samo streho, če ni bila ravna
                                    hisa.updateVidneStene();
                                    ustvari_hiso(hisa, scena);
                                    renderer.render(SCENA, camera);
                                }
                            });

                            ravnina.on('touchend', function (ev) {
                                if (pripraviSteno(ev) == 1) {
                                    hisa.uniciObjekte(); //TODO izbrisati samo streho, če ni bila ravna
                                    hisa.updateVidneStene();
                                    ustvari_hiso(hisa, scena);
                                    renderer.render(SCENA, camera);
                                }
                            });
                            scena.add(ravnina);

                            hisa.obod.push(ravnina);

                            var ravninaMere = new THREE.PlaneGeometry(50, SIRINA);
                            ravninaMere.rotateX(Math.PI / 2);
                            ravninaMere.rotateY(Math.PI);
                            ravninaMere.rotateZ(Math.PI);

                            var tekstura = LOADER.load('./images/dolzina1200.png');
                            var ravninaMat = new THREE.MeshBasicMaterial({
                                side: THREE.DoubleSide,
                                map: tekstura,
                                transparent: true,
                                alphaTest: 0.5
                            });
                            var ravnina = new THREE.Mesh(ravninaMere, ravninaMat);
                            ravnina.position.set(t + DOLZINA / 2 + 50,-(VISINA/2)+1,v);
                            scena.add(ravnina);

                        }
                        if (element.stene[k].jeCelna()){
                            // delamo tri plosce
                            for (var l = 0; l < 3; l++) {

                                var ravninaGeo = new THREE.PlaneGeometry(SIRINA, VISINA);
                                if (k == 3) {
                                    ravninaGeo.rotateY(Math.PI);
                                }

                                if (element.stene[k].imaOkno(l)) {
                                    var tekstura = LOADER.load(element.stene[k].getOkno(l));
                                    var ravninaMat = new THREE.MeshBasicMaterial({
                                        side: THREE.DoubleSide,
                                        map: tekstura,
                                        transparent: true,
                                        alphaTest: 0.5
                                    });
                                } else {
                                    var ravninaMat = new THREE.MeshBasicMaterial({
                                            color: 0xffff00,
                                            side: THREE.DoubleSide,
                                            transparent: true,
                                            opacity: 0
                                        });
                                }

                                var ravnina = new THREE.Mesh(ravninaGeo, ravninaMat);
                                //nastavim pozicijo

                                ravnina.position.set(t+(l-1)*SIRINA, 0, posZ[k]);
                                ravnina.name = i.toString() + "_" + j.toString() + "_" + k.toString() + "_" + l.toString();

                                //dodamo crte na celne ploskve
                                var geometrija = new THREE.EdgesGeometry( ravnina.geometry );
                                var crtaMaterial = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 1 } );
                                var wireframe = new THREE.LineSegments( geometrija, crtaMaterial );
                                wireframe.renderOrder = 2; // make sure wireframes are rendered 2nd
                                ravnina.add( wireframe );



                                //Interaktivnost
                                ravnina.cursor = 'pointer';

                                ravnina.on('click', function (ev) {
                                    if (pripraviSteno(ev) == 1) {
                                        hisa.uniciObjekte(); //TODO izbrisati samo streho, če ni bila ravna
                                        hisa.updateVidneStene();
                                        ustvari_hiso(hisa, scena);
                                        renderer.render(SCENA, camera);
                                    }
                                });

                                ravnina.on('touchend', function (ev) {
                                    if (pripraviSteno(ev) == 1) {
                                        hisa.uniciObjekte(); //TODO izbrisati samo streho, če ni bila ravna
                                        hisa.updateVidneStene();
                                        ustvari_hiso(hisa, scena);
                                        renderer.render(SCENA, camera);
                                    }
                                });
                                scena.add(ravnina);
                                hisa.obod.push(ravnina);
                            }

                            var ravninaMere = new THREE.PlaneGeometry(50, DOLZINA);
                            ravninaMere.rotateX(Math.PI / 2);
                            ravninaMere.rotateY(-Math.PI / 2);
                            ravninaMere.rotateZ(Math.PI);

                            var tekstura = LOADER.load('./images/dolzina4000.png');
                            var ravninaMat = new THREE.MeshBasicMaterial({
                                side: THREE.DoubleSide,
                                map: tekstura,
                                transparent: true,
                                alphaTest: 0.5
                            });
                            ravnina = new THREE.Mesh(ravninaMere, ravninaMat);
                            ravnina.position.set(t,-(VISINA/2)+1,SIRINA);
                            scena.add(ravnina);
                        }
                    }


                }


                if (element.streha.getVrsta() == 2) {
                    var streha = generirajStreha(geometrijaDvokapnica(), t, v);
                    scena.add(streha);
                    hisa.ostresje.push(streha);
                    var mrezaStreha = generirajStrehaMreza(geometrijaDvokapnica());
                    streha.add(wireframe);
                }


                hisa.elementi.push(cube);

            }
        }
    }

}

/**
 * metoda pripravi podlago na sceni. Podlaga so pravokotniki s črnim robom, ki so aktivni na klik.
 * @param scena  scena, na katero dodajamo mrezo
 */
function pripraviPodlago(scena){
    for (i = -10; i <= 10; i++){
        for (j = -10; j <= 10; j++){

            //pripravimo en raster
            var rasterGeometrija = new THREE.PlaneGeometry(DOLZINA, SIRINA);
            rasterGeometrija.rotateX(Math.PI / 2);
            rasterGeometrija.rotateY(Math.PI);
            rasterGeometrija.rotateZ(Math.PI);
            var ravninaMaterial = new THREE.MeshBasicMaterial({
                color: 0xffff00,
                side: THREE.DoubleSide,
                transparent: true,
                opacity: 0
            });

            var rasterMesh = new THREE.Mesh(rasterGeometrija, ravninaMaterial);
            //nastavim pozicijo
            rasterMesh.position.set(i * DOLZINA,-VISINA/2+2,j * SIRINA);
            //TODO na plosci z merami, ni mogoče klikniti
            rasterMesh.name = "podlaga_" + i.toString() + "_" + j.toString();

            var geometrija = new THREE.EdgesGeometry( rasterMesh.geometry );
            var crtaMaterial = new THREE.LineBasicMaterial( { color: 0x000000, linewidth: 1 } );
            var wireframe = new THREE.LineSegments( geometrija, crtaMaterial );
            wireframe.renderOrder = 2; // make sure wireframes are rendered 2nd
            rasterMesh.add( wireframe );

            //Interaktivnost
            rasterMesh.cursor = 'pointer';
            rasterMesh.on('mousedown', function (ev) {
                MOUSEDOWN = true;
                MOUSEMOVE = false;
            });

            rasterMesh.on('mousemove', function (ev) {
                if (MOUSEDOWN) {
                    MOUSEMOVE = true;
                }
            });

            rasterMesh.on('mouseup', function (ev) {
                // MOUSEMOVE potreben, da lahko vrtimo, a ne dodamo elementa
                if (MOUSEMOVE) {
                    MOUSEDOWN = false;
                    MOUSEDOWN = false;
                } else {
                    console.log(ev.data.target.name);
                }
                //TODO doda se element v objekt
            });

            scena.add(rasterMesh);

        }
    }



}